<?php

spl_autoload_register('Autoloader::autoloadConfig');
spl_autoload_register('Autoloader::autoloadTrait');
spl_autoload_register('Autoloader::autoloadDto');
spl_autoload_register('Autoloader::autoloadDao');
spl_autoload_register('Autoloader::autoloadLib');

class Autoloader{

    static function autoloadConfig($class){

        $file = 'configs/' .lcfirst($class).'.php';
        if(is_file($file) && is_readable($file))
        {
            require $file;
        }
    }

    static function autoloadTrait($class){

        $file = 'modeles/traits/' .lcfirst($class).'.php';
        if(is_file($file) && is_readable($file))
        {
            require $file;
        }
    }

    static function autoloadDto($class){

        $file = 'modeles/dto/' .ucfirst($class).'.php';
        if(is_file($file) && is_readable($file))
        {
            require $file;
        }
    }

    static function autoloadDao($class){

        $file = 'modeles/dao/' .lcfirst($class).'.php';
        if(is_file($file) && is_readable($file))
        {
            require $file;
        }
    }

    static function autoloadLib($class){

        $file = 'fonctions/' .lcfirst($class).'.php';
        if(is_file($file) && is_readable($file))
        {
            require $file;
        }
    }
}