<div class="conteneur">
	<header>
		<?php include 'header.php' ;?>
	</header>
	<main>
		<div id="content">
		<div id="gauche">
		<?php
		$menuRavageur->creerListeRavageurs($_SESSION['menuRavageur']);
		?>
		</div>
		<div id="droite">
			<?php
		$infoRavageur->creerInfoRavageur($nomRavageur,$stadeActifRavageur,$photoRavageur,$nombreGenerationsRavageur,$periodeRisqueRavageur,$symptomesRavageur,$stadeSensibleRavageur);
		echo($affichage);
		?>
		</div>
	</div>
	</main>
	<footer class="bg-light">
		<?php include 'footer.php' ;?>
	</footer>
</div>