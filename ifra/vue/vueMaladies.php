<div class="conteneur">
	<header>
		<?php include 'header.php' ;?>
	</header>
	<main>
		<div id="content">
		<div id="gauche">
		<?php
		$menuMaladie->creerListeMaladies($_SESSION['menuMaladie']);
		?>
		</div>
		<div id="droite">
			<?php
		$infoMaladie->creerInfoMaladie($nomMaladie,$conditionsFavorablesMaladie,$photoMaladie,$periodeRisqueMaladie,$symptomesMaladie,$stadeSensibleMaladie);
		echo($affichage);
		?>
		</div>
	</div>
	</main>
	<footer class="bg-light">
		<?php include 'footer.php' ;?>
	</footer>
</div>