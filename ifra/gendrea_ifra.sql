-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 09 Novembre 2018 à 17:02
-- Version du serveur :  10.1.26-MariaDB-0+deb9u1
-- Version de PHP :  7.0.19-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gendrea_ifra2`
--

-- --------------------------------------------------------

--
-- Structure de la table `AFFECTER`
--

CREATE TABLE `AFFECTER` (
  `idBioAgresseur` int(11) NOT NULL,
  `idOrgane` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `BIOAGRESSEUR`
--

CREATE TABLE `BIOAGRESSEUR` (
  `idBioAgresseur` int(11) NOT NULL,
  `nomBioAgresseur` varchar(40) DEFAULT NULL,
  `typeBioAgresseur` varchar(30) DEFAULT NULL,
  `periodeRisqueBioAgresseur` varchar(50) DEFAULT NULL,
  `symptomesBioAgresseur` text,
  `stadeSensibleBioAgresseur` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `BIOAGRESSEUR`
--

INSERT INTO `BIOAGRESSEUR` (`idBioAgresseur`, `nomBioAgresseur`, `typeBioAgresseur`, `periodeRisqueBioAgresseur`, `symptomesBioAgresseur`, `stadeSensibleBioAgresseur`) VALUES
(1, 'Malaria', 'M', 'jsp', 'chute de feuilles', 'jsp'),
(2, 'Cétoine dorée', 'R', 'jsp', 'jsp', 'jsp');

-- --------------------------------------------------------

--
-- Structure de la table `DATEOBSERV`
--

CREATE TABLE `DATEOBSERV` (
  `dateObservation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `DATEOBSERV`
--

INSERT INTO `DATEOBSERV` (`dateObservation`) VALUES
('2018-10-05');

-- --------------------------------------------------------

--
-- Structure de la table `DEPARTEMENT`
--

CREATE TABLE `DEPARTEMENT` (
  `departement_code` varchar(3) CHARACTER SET utf8 NOT NULL,
  `departement_nom` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `departement_nom_uppercase` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `DEPARTEMENT`
--

INSERT INTO `DEPARTEMENT` (`departement_code`, `departement_nom`, `departement_nom_uppercase`) VALUES
('01', 'Ain', 'AIN'),
('02', 'Aisne', 'AISNE'),
('03', 'Allier', 'ALLIER'),
('05', 'Hautes-Alpes', 'HAUTES-ALPES'),
('04', 'Alpes-de-Haute-Provence', 'ALPES-DE-HAUTE-PROVENCE'),
('06', 'Alpes-Maritimes', 'ALPES-MARITIMES'),
('07', 'Ardèche', 'ARDÈCHE'),
('08', 'Ardennes', 'ARDENNES'),
('09', 'Ariège', 'ARIÈGE'),
('10', 'Aube', 'AUBE'),
('11', 'Aude', 'AUDE'),
('12', 'Aveyron', 'AVEYRON'),
('13', 'Bouches-du-Rhône', 'BOUCHES-DU-RHÔNE'),
('14', 'Calvados', 'CALVADOS'),
('15', 'Cantal', 'CANTAL'),
('16', 'Charente', 'CHARENTE'),
('17', 'Charente-Maritime', 'CHARENTE-MARITIME'),
('18', 'Cher', 'CHER'),
('19', 'Corrèze', 'CORRÈZE'),
('2a', 'Corse-du-sud', 'CORSE-DU-SUD'),
('2b', 'Haute-corse', 'HAUTE-CORSE'),
('21', 'Côte-d\'or', 'CÔTE-D\'OR'),
('22', 'Côtes-d\'armor', 'CÔTES-D\'ARMOR'),
('23', 'Creuse', 'CREUSE'),
('24', 'Dordogne', 'DORDOGNE'),
('25', 'Doubs', 'DOUBS'),
('26', 'Drôme', 'DRÔME'),
('27', 'Eure', 'EURE'),
('28', 'Eure-et-Loir', 'EURE-ET-LOIR'),
('29', 'Finistère', 'FINISTÈRE'),
('30', 'Gard', 'GARD'),
('31', 'Haute-Garonne', 'HAUTE-GARONNE'),
('32', 'Gers', 'GERS'),
('33', 'Gironde', 'GIRONDE'),
('34', 'Hérault', 'HÉRAULT'),
('35', 'Ile-et-Vilaine', 'ILE-ET-VILAINE'),
('36', 'Indre', 'INDRE'),
('37', 'Indre-et-Loire', 'INDRE-ET-LOIRE'),
('38', 'Isère', 'ISÈRE'),
('39', 'Jura', 'JURA'),
('40', 'Landes', 'LANDES'),
('41', 'Loir-et-Cher', 'LOIR-ET-CHER'),
('42', 'Loire', 'LOIRE'),
('43', 'Haute-Loire', 'HAUTE-LOIRE'),
('44', 'Loire-Atlantique', 'LOIRE-ATLANTIQUE'),
('45', 'Loiret', 'LOIRET'),
('46', 'Lot', 'LOT'),
('47', 'Lot-et-Garonne', 'LOT-ET-GARONNE'),
('48', 'Lozère', 'LOZÈRE'),
('49', 'Maine-et-Loire', 'MAINE-ET-LOIRE'),
('50', 'Manche', 'MANCHE'),
('51', 'Marne', 'MARNE'),
('52', 'Haute-Marne', 'HAUTE-MARNE'),
('53', 'Mayenne', 'MAYENNE'),
('54', 'Meurthe-et-Moselle', 'MEURTHE-ET-MOSELLE'),
('55', 'Meuse', 'MEUSE'),
('56', 'Morbihan', 'MORBIHAN'),
('57', 'Moselle', 'MOSELLE'),
('58', 'Nièvre', 'NIÈVRE'),
('59', 'Nord', 'NORD'),
('60', 'Oise', 'OISE'),
('61', 'Orne', 'ORNE'),
('62', 'Pas-de-Calais', 'PAS-DE-CALAIS'),
('63', 'Puy-de-Dôme', 'PUY-DE-DÔME'),
('64', 'Pyrénées-Atlantiques', 'PYRÉNÉES-ATLANTIQUES'),
('65', 'Hautes-Pyrénées', 'HAUTES-PYRÉNÉES'),
('66', 'Pyrénées-Orientales', 'PYRÉNÉES-ORIENTALES'),
('67', 'Bas-Rhin', 'BAS-RHIN'),
('68', 'Haut-Rhin', 'HAUT-RHIN'),
('69', 'Rhône', 'RHÔNE'),
('70', 'Haute-Saône', 'HAUTE-SAÔNE'),
('71', 'Saône-et-Loire', 'SAÔNE-ET-LOIRE'),
('72', 'Sarthe', 'SARTHE'),
('73', 'Savoie', 'SAVOIE'),
('74', 'Haute-Savoie', 'HAUTE-SAVOIE'),
('75', 'Paris', 'PARIS'),
('76', 'Seine-Maritime', 'SEINE-MARITIME'),
('77', 'Seine-et-Marne', 'SEINE-ET-MARNE'),
('78', 'Yvelines', 'YVELINES'),
('79', 'Deux-Sèvres', 'DEUX-SÈVRES'),
('80', 'Somme', 'SOMME'),
('81', 'Tarn', 'TARN'),
('82', 'Tarn-et-Garonne', 'TARN-ET-GARONNE'),
('83', 'Var', 'VAR'),
('84', 'Vaucluse', 'VAUCLUSE'),
('85', 'Vendée', 'VENDÉE'),
('86', 'Vienne', 'VIENNE'),
('87', 'Haute-Vienne', 'HAUTE-VIENNE'),
('88', 'Vosges', 'VOSGES'),
('89', 'Yonne', 'YONNE'),
('90', 'Territoire de Belfort', 'TERRITOIRE DE BELFORT'),
('91', 'Essonne', 'ESSONNE'),
('92', 'Hauts-de-Seine', 'HAUTS-DE-SEINE'),
('93', 'Seine-Saint-Denis', 'SEINE-SAINT-DENIS'),
('94', 'Val-de-Marne', 'VAL-DE-MARNE'),
('95', 'Val-d\'oise', 'VAL-D\'OISE'),
('976', 'Mayotte', 'MAYOTTE'),
('971', 'Guadeloupe', 'GUADELOUPE'),
('973', 'Guyane', 'GUYANE'),
('972', 'Martinique', 'MARTINIQUE'),
('974', 'Réunion', 'RÉUNION');

-- --------------------------------------------------------

--
-- Structure de la table `MALADIE`
--

CREATE TABLE `MALADIE` (
  `idBioAgresseur` int(11) NOT NULL,
  `conditionsFavorables` varchar(40) DEFAULT NULL,
  `nomBioAgresseur` varchar(40) DEFAULT NULL,
  `typeBioAgresseur` varchar(30) DEFAULT NULL,
  `periodeRisqueBioAgresseur` varchar(50) DEFAULT NULL,
  `symptomesBioAgresseur` text,
  `stadeSensibleBioAgresseur` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `MALADIE`
--

INSERT INTO `MALADIE` (`idBioAgresseur`, `conditionsFavorables`, `nomBioAgresseur`, `typeBioAgresseur`, `periodeRisqueBioAgresseur`, `symptomesBioAgresseur`, `stadeSensibleBioAgresseur`) VALUES
(1, 'chaleur', 'Malaria', 'M', 'jsp', 'chute de feuilles', 'jsp');

-- --------------------------------------------------------

--
-- Structure de la table `OBSERVATION`
--

CREATE TABLE `OBSERVATION` (
  `idObservation` int(11) NOT NULL,
  `idBioAgresseur` int(11) NOT NULL,
  `idPlante` int(11) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `dateObservation` date NOT NULL,
  `descriptifObservation` text,
  `codeDepartement` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `OBSERVATION`
--

INSERT INTO `OBSERVATION` (`idObservation`, `idBioAgresseur`, `idPlante`, `idUtilisateur`, `dateObservation`, `descriptifObservation`, `codeDepartement`) VALUES
(1, 1, 1, 2, '2018-10-05', 'fefszf', '33');

-- --------------------------------------------------------

--
-- Structure de la table `ORGANES`
--

CREATE TABLE `ORGANES` (
  `idOrgane` int(11) NOT NULL,
  `nomOrgane` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `ORGANES`
--

INSERT INTO `ORGANES` (`idOrgane`, `nomOrgane`) VALUES
(1, 'Racine'),
(2, 'Tige'),
(3, 'Feuille'),
(4, 'Graine'),
(5, 'Pétale');

-- --------------------------------------------------------

--
-- Structure de la table `PLANTE`
--

CREATE TABLE `PLANTE` (
  `idPlante` int(11) NOT NULL,
  `nomPlante` varchar(128) DEFAULT NULL,
  `descriptifPlante` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `PLANTE`
--

INSERT INTO `PLANTE` (`idPlante`, `nomPlante`, `descriptifPlante`) VALUES
(1, 'Begonia', 'Les bégonias sont des plantes à fleurs du genre Begonia. Celui-ci représente la quasi-totalité de la famille des Bégoniacées, avec plus de 1500 espèces, et sont la plupart du temps facilement reconnaissables à leurs feuilles asymétriques et aux fragiles fleurs à minuscule cœur jaune, parfois très dissemblables sur un même pied, selon qu\'elles sont mâles ou femelles, puisque ce sont des plantes monoïques ; tandis que les autres caractères apparaissent au contraire très variables d\'une espèce à l\'autre, tant la forme et la couleur du feuillage et des fleurs, que par le port ou la hauteur, qui s\'échelonne de quelques centimètres à 3 ou 4 mètres. À ces espèces botaniques, il faut ajouter presque autant d\'hybrides et de multiples cultivars horticoles.'),
(2, 'Marguerite', 'La Marguerite ou Marguerite commune (Leucanthemum vulgare) est une plante herbacée vivace de la famille des Asteraceae.\r\nC\'est une plante à fleur en touffe, à tige érigée, ridée, aux feuilles basales pétiolées, et aux caulinaires engainantes crénelées (alors que celles de la pâquerette ont 4 à 7 dents par côté). Les inflorescences sont de grands capitules aux ligules blanches autour du centre jaune, lui-même composé de nombreuses petites fleurs sessiles ou fleurons.\r\n\r\nElle se rencontre dans les prés, accotements, bois clairs, sur substrat calcaire à légèrement acide ; c\'est une plante très commune dans toute l\'Europe jusque dans les régions septentrionales sauf au Spitzberg.'),
(3, 'Coquelicot', 'Le coquelicot (Papaver rhoeas) est une espèce de plantes dicotylédones de la famille des Papaveraceae, originaire d\'Eurasie.\r\n\r\nC\'est une plante herbacée annuelle, très abondante dans les terrains fraîchement remués à partir du printemps, qui se distingue par la couleur rouge de ses fleurs et par le fait qu\'elle forme souvent de grands tapis colorés visibles de très loin. Elle appartient au groupe des plantes dites messicoles car elle est associée à l\'agriculture depuis des temps très anciens, grâce à son cycle biologique adapté aux cultures de céréales, la floraison et la mise à graines intervenant avant la moisson. Très commune dans différents pays d\'Europe, elle a beaucoup régressé du fait de l\'emploi généralisé des herbicides et de l\'amélioration de la propreté des semences de céréales.');

-- --------------------------------------------------------

--
-- Structure de la table `RAVAGEUR`
--

CREATE TABLE `RAVAGEUR` (
  `idBioAgresseur` int(11) NOT NULL,
  `stadeActif` varchar(40) DEFAULT NULL,
  `nbGenerations` int(11) DEFAULT NULL,
  `nomBioAgresseur` varchar(40) DEFAULT NULL,
  `typeBioAgresseur` varchar(30) DEFAULT NULL,
  `periodeRisqueBioAgresseur` varchar(50) DEFAULT NULL,
  `symptomesBioAgresseur` text,
  `stadeSensibleBioAgresseur` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `RAVAGEUR`
--

INSERT INTO `RAVAGEUR` (`idBioAgresseur`, `stadeActif`, `nbGenerations`, `nomBioAgresseur`, `typeBioAgresseur`, `periodeRisqueBioAgresseur`, `symptomesBioAgresseur`, `stadeSensibleBioAgresseur`) VALUES
(2, 'test', 3, 'Cétoine dorée', 'R', 'jsp', 'jsp', 'jsp');

-- --------------------------------------------------------

--
-- Structure de la table `SENSIBLE`
--

CREATE TABLE `SENSIBLE` (
  `idBioAgresseur` int(11) NOT NULL,
  `idPlante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `SENSIBLE`
--

INSERT INTO `SENSIBLE` (`idBioAgresseur`, `idPlante`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `TRAITEMENT`
--

CREATE TABLE `TRAITEMENT` (
  `idTraitement` int(2) NOT NULL,
  `libelleTraitement` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `TRAITER`
--

CREATE TABLE `TRAITER` (
  `idBioAgresseur` int(11) NOT NULL,
  `idTraitement` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `TYPE_UTILISATEUR`
--

CREATE TABLE `TYPE_UTILISATEUR` (
  `codeType` varchar(128) NOT NULL,
  `libelleType` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `TYPE_UTILISATEUR`
--

INSERT INTO `TYPE_UTILISATEUR` (`codeType`, `libelleType`) VALUES
('DIR', 'Directeur'),
('OBS', 'Observateur'),
('SLR', 'Salarie');

-- --------------------------------------------------------

--
-- Structure de la table `UTILISATEUR`
--

CREATE TABLE `UTILISATEUR` (
  `idUtilisateur` int(11) NOT NULL,
  `codeType` varchar(128) NOT NULL,
  `login` varchar(128) DEFAULT NULL,
  `mdp` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `UTILISATEUR`
--

INSERT INTO `UTILISATEUR` (`idUtilisateur`, `codeType`, `login`, `mdp`) VALUES
(1, 'DIR', 'louis.michon@free.fr', 'cfdb661d0bf483ad1c69eeb9c6ec1463'),
(2, 'OBS', 'yann.martin@matin.fr', 'a2eb88ffd63fb45c579933d80bc5d4c3'),
(3, 'SLR', 'alex.gendre@gmx.fr', '1c64c83712717110f09344aa6dfde5da'),
(4, 'OBS', 'test@test.com', '098f6bcd4621d373cade4e832627b4f6');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `AFFECTER`
--
ALTER TABLE `AFFECTER`
  ADD PRIMARY KEY (`idBioAgresseur`,`idOrgane`),
  ADD KEY `I_FK_AFFECTER_BIOAGRESSEUR` (`idBioAgresseur`),
  ADD KEY `I_FK_AFFECTER_ORGANES` (`idOrgane`);

--
-- Index pour la table `BIOAGRESSEUR`
--
ALTER TABLE `BIOAGRESSEUR`
  ADD PRIMARY KEY (`idBioAgresseur`);

--
-- Index pour la table `DATEOBSERV`
--
ALTER TABLE `DATEOBSERV`
  ADD PRIMARY KEY (`dateObservation`);

--
-- Index pour la table `DEPARTEMENT`
--
ALTER TABLE `DEPARTEMENT`
  ADD PRIMARY KEY (`departement_code`),
  ADD KEY `departement_code` (`departement_code`);

--
-- Index pour la table `MALADIE`
--
ALTER TABLE `MALADIE`
  ADD PRIMARY KEY (`idBioAgresseur`);

--
-- Index pour la table `OBSERVATION`
--
ALTER TABLE `OBSERVATION`
  ADD PRIMARY KEY (`idObservation`),
  ADD KEY `I_FK_OBSERVER_BIOAGRESSEUR` (`idBioAgresseur`),
  ADD KEY `I_FK_OBSERVER_PLANTE` (`idPlante`),
  ADD KEY `I_FK_OBSERVER_UTILISATEUR` (`idUtilisateur`),
  ADD KEY `I_FK_OBSERVER_DATEOBSERV` (`dateObservation`),
  ADD KEY `codeDepartement` (`codeDepartement`);

--
-- Index pour la table `ORGANES`
--
ALTER TABLE `ORGANES`
  ADD PRIMARY KEY (`idOrgane`);

--
-- Index pour la table `PLANTE`
--
ALTER TABLE `PLANTE`
  ADD PRIMARY KEY (`idPlante`);

--
-- Index pour la table `RAVAGEUR`
--
ALTER TABLE `RAVAGEUR`
  ADD PRIMARY KEY (`idBioAgresseur`);

--
-- Index pour la table `SENSIBLE`
--
ALTER TABLE `SENSIBLE`
  ADD PRIMARY KEY (`idBioAgresseur`,`idPlante`),
  ADD KEY `I_FK_SENSIBLE_BIOAGRESSEUR` (`idBioAgresseur`),
  ADD KEY `I_FK_SENSIBLE_PLANTE` (`idPlante`);

--
-- Index pour la table `TRAITEMENT`
--
ALTER TABLE `TRAITEMENT`
  ADD PRIMARY KEY (`idTraitement`);

--
-- Index pour la table `TRAITER`
--
ALTER TABLE `TRAITER`
  ADD PRIMARY KEY (`idBioAgresseur`,`idTraitement`),
  ADD KEY `I_FK_TRAITER_BIOAGRESSEUR` (`idBioAgresseur`),
  ADD KEY `I_FK_TRAITER_TRAITEMENT` (`idTraitement`);

--
-- Index pour la table `TYPE_UTILISATEUR`
--
ALTER TABLE `TYPE_UTILISATEUR`
  ADD PRIMARY KEY (`codeType`);

--
-- Index pour la table `UTILISATEUR`
--
ALTER TABLE `UTILISATEUR`
  ADD PRIMARY KEY (`idUtilisateur`),
  ADD KEY `I_FK_UTILISATEUR_TYPE_UTILISATEUR` (`codeType`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `BIOAGRESSEUR`
--
ALTER TABLE `BIOAGRESSEUR`
  MODIFY `idBioAgresseur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `OBSERVATION`
--
ALTER TABLE `OBSERVATION`
  MODIFY `idObservation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `ORGANES`
--
ALTER TABLE `ORGANES`
  MODIFY `idOrgane` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `PLANTE`
--
ALTER TABLE `PLANTE`
  MODIFY `idPlante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `TRAITEMENT`
--
ALTER TABLE `TRAITEMENT`
  MODIFY `idTraitement` int(2) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `UTILISATEUR`
--
ALTER TABLE `UTILISATEUR`
  MODIFY `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `AFFECTER`
--
ALTER TABLE `AFFECTER`
  ADD CONSTRAINT `FK_AFFECTER_BIOAGRESSEUR` FOREIGN KEY (`IDBIOAGRESSEUR`) REFERENCES `BIOAGRESSEUR` (`IDBIOAGRESSEUR`),
  ADD CONSTRAINT `FK_AFFECTER_ORGANES` FOREIGN KEY (`IDORGANE`) REFERENCES `ORGANES` (`IDORGANE`);

--
-- Contraintes pour la table `MALADIE`
--
ALTER TABLE `MALADIE`
  ADD CONSTRAINT `FK_MALADIE_BIOAGRESSEUR` FOREIGN KEY (`idBioAgresseur`) REFERENCES `BIOAGRESSEUR` (`IDBIOAGRESSEUR`);

--
-- Contraintes pour la table `OBSERVATION`
--
ALTER TABLE `OBSERVATION`
  ADD CONSTRAINT `FK_OBSERVER_BIOAGRESSEUR` FOREIGN KEY (`IDBIOAGRESSEUR`) REFERENCES `BIOAGRESSEUR` (`IDBIOAGRESSEUR`),
  ADD CONSTRAINT `FK_OBSERVER_DATEOBSERV` FOREIGN KEY (`DATEOBSERVATION`) REFERENCES `DATEOBSERV` (`DATEOBSERVATION`),
  ADD CONSTRAINT `FK_OBSERVER_PLANTE` FOREIGN KEY (`IDPLANTE`) REFERENCES `PLANTE` (`IDPLANTE`),
  ADD CONSTRAINT `FK_OBSERVER_UTILISATEUR` FOREIGN KEY (`IDUTILISATEUR`) REFERENCES `UTILISATEUR` (`IDUTILISATEUR`);

--
-- Contraintes pour la table `RAVAGEUR`
--
ALTER TABLE `RAVAGEUR`
  ADD CONSTRAINT `FK_RAVAGEUR_BIOAGRESSEUR` FOREIGN KEY (`idBioAgresseur`) REFERENCES `BIOAGRESSEUR` (`IDBIOAGRESSEUR`);

--
-- Contraintes pour la table `SENSIBLE`
--
ALTER TABLE `SENSIBLE`
  ADD CONSTRAINT `FK_SENSIBLE_BIOAGRESSEUR` FOREIGN KEY (`IDBIOAGRESSEUR`) REFERENCES `BIOAGRESSEUR` (`IDBIOAGRESSEUR`),
  ADD CONSTRAINT `FK_SENSIBLE_PLANTE` FOREIGN KEY (`IDPLANTE`) REFERENCES `PLANTE` (`IDPLANTE`);

--
-- Contraintes pour la table `TRAITER`
--
ALTER TABLE `TRAITER`
  ADD CONSTRAINT `FK_TRAITER_BIOAGRESSEUR` FOREIGN KEY (`IDBIOAGRESSEUR`) REFERENCES `BIOAGRESSEUR` (`IDBIOAGRESSEUR`),
  ADD CONSTRAINT `FK_TRAITER_TRAITEMENT` FOREIGN KEY (`IDTRAITEMENT`) REFERENCES `TRAITEMENT` (`IDTRAITEMENT`);

--
-- Contraintes pour la table `UTILISATEUR`
--
ALTER TABLE `UTILISATEUR`
  ADD CONSTRAINT `FK_UTILISATEUR_TYPE_UTILISATEUR` FOREIGN KEY (`CODETYPE`) REFERENCES `TYPE_UTILISATEUR` (`CODETYPE`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
