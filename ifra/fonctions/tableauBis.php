<?php
class tableau {
	
	private $nbCol;
	private $nbLig;
	private $titreTab;
	private $titreCols;
	private $titreCol;
	private $donneesTab;
	private $idTab;


	public function __construct($unIdTab , $lesDonnees){
		$this->idTab = $unIdTab;
		$this->donneesTab = $lesDonnees;
	}
			
	public function setTitreTab($unTitreTab){
		$this->titreTab = $unTitreTab;
	}
	
	public function setTitreCols($lesTitreCols){
		$this->titreCols = $lesTitreCols;
	}
	
	public function setTitreCol($lesTitreCol){
		$this->titreCol = $lesTitreCol;
	}

	public function getDonneesTab(){
		return $this->donneesTab;
	}
	
	
	public function afficheTableau(){
		
		$tabHeadChaine ="";
		$tabChaine = "<table class='table' id = '". $this->idTab . "' >";
		
		//Afficher le titre du tableau
		if(!empty($this->titreTab)){
			$tabHeadChaine =  "<thead class='thead-dark'> <tr > <th scope='row' colspan = '";
			$tabHeadChaine .= count($this->donneesTab[0]);
			$tabHeadChaine .= "' >";
			$tabHeadChaine .= $this->titreTab;
			$tabHeadChaine .= "</th></tr></thead>";
		}
		
		
		//Afficher les entêtes des colonnes
		if(!empty($this->titreCol)){
			$tabHeadChaine .= "<thead class='thead-dark'><tr>";
			foreach($this->titreCol as $cellule){
				$tabHeadChaine .= "<th scope='row'>";
				$tabHeadChaine .= $cellule;
				$tabHeadChaine .= "</th>";
			}
			$tabHeadChaine .= "</tr></thead>";
		}
		
		//Afficher le corps du tableau
		$tabBodyChaine = "<tbody>";

		foreach($this->donneesTab as $ligne){
			$tabBodyChaine .=  "<tr>";
			$ligne[] = "toto";

			$ligne[] = "jkgkjg";
			foreach($ligne as $cellule){
				$tabBodyChaine .=  "<td>";
				$tabBodyChaine .=  $cellule;
				$tabBodyChaine .=  "</td>";
			}
			$tabBodyChaine .=  "</tr>";
		}
		$tabBodyChaine .=  "</tbody></table>";
		
		$tabChaine .= $tabHeadChaine . $tabBodyChaine;
		
		echo $tabChaine;	
	}
	
}