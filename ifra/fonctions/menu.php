<?php
/**
 * Classe Menu
 * Permet de créer des menus
 * $style style css
 * $composants liste des items coposant le menu
 */
class Menu{
	private $style;
	private $composants = array();

	/**
	 * Constructeur de la classe Menu
	 * @param $unStyle (style pour css)
	 */
	public function __construct($unStyle ){
		$this->style = $unStyle;
	}

	/**
	 * Ajoute un item à la liste des items du menu
	 * @param $unComposant (un item du menu)
	 */
	public function ajouterComposant($unComposant){
		$this->composants[] = $unComposant;
	}


	/**
	 * Crée un nouvelle item pour le menu
	 * @param $unLien  (valeur transmise)
	 * @param $uneValeur (valeur affichée)
	 * @return un item pour le menu
	 */
	public function creerItemLien($unLien,$uneValeur){
		$composant = array();
		$composant[0] = $unLien ;
		$composant[1] = $uneValeur ;
		return $composant;
	}

	/**
	 * crée le menu à afficher
	 * @param $composantActif (item sélectionné)
	 * @param $nomMenu (nom variable transmise)
	 */
	public function creerMenu($composantActif,$nomMenu){

		$menu = "<nav class=\"navbar navbar-expand-lg navbar-light\" style=\"background-color: #40A497;\">
		<a class=\"navbar-brand\" href=\"#\">Institut Francais de Recherche Agronomique</a>
		<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
		  <span class=\"navbar-toggler-icon\"></span>
		</button>
		<div class=\"collapse navbar-collapse\" id=\"navbarNav\">";

		$menu .= "<ul class = 'navbar-nav'>";
		foreach($this->composants as $composant){
			if($composant[0] == $composantActif){
				$menu .= "<li class='nav-item active'>";
				$menu .= "<a class='nav-link' href='index.php?".$nomMenu."=".$composant[0]."'>";
				$menu .=  $composant[1] . "<span class='sr-only'>(current)</span>";
				$menu .= "</a>";
			}
			else{
				$menu .= "<li class='nav-item'>";
				$menu .= "<a class='nav-link' href='index.php?" . $nomMenu ;
				$menu .= "=" . $composant[0] . "' >";
				$menu .= $composant[1];
				$menu .= "</a>";
			}
			$menu .= "</li>";
		}
		$menu .= "</ul>";
		$menu .= "</div>";
		$menu .= "</nav>";
		return $menu ;
	}
	public function creerListe($composantActif){
	    $menu = "<div class='container'>";
	    $menu .= "<div class='list-group'>";
	    $menu .= "<ul class = '" .  $this->style . "'>";
		foreach($this->composants as $composant){
		    $menu .= "<div class='row'>";
		    $menu .= " <div class='col-3'>";
			if($composant[0] == $composantActif){

				$menu .=  "<span class='list-group-item active'" . $composant[1] ."</span>";
			}
			else{

				$menu .= "<a href='index.php?menuPlante";
				$menu .= "=" . $composant[0] . "' >";
				$menu .= "<span class='list-group-item'>" . $composant[1] ."</span>";
				$menu .= "</a>";
			}

			$menu .= "</div>";
			$menu .= "</div>";
		}
		$menu .= "</ul>";
		$menu .= "</div>";
		$menu .= "</div>";
		
	    echo $menu ;
	}

	public function creerListeMaladies($composantActif){
	    $menu = "<div class='container'>";
	    $menu .= "<div class='list-group'>";
	    $menu .= "<ul class = '" .  $this->style . "'>";
		foreach($this->composants as $composant){
		    $menu .= "<div class='row'>";
		    $menu .= " <div class='col-3'>";
			if($composant[0] == $composantActif){

				$menu .=  "<span class='list-group-item active'" . $composant[1] ."</span>";
			}
			else{

				$menu .= "<a href='index.php?menuMaladie";
				$menu .= "=" . $composant[0] . "' >";
				$menu .= "<span class='list-group-item'>" . $composant[1] ."</span>";
				$menu .= "</a>";
			}

			$menu .= "</div>";
			$menu .= "</div>";
		}
		$menu .= "</ul>";
		$menu .= "</div>";
		$menu .= "</div>";
		
	    echo $menu ;
	}
    
	public function creerInfoPlante($nomPlante,$descPlante,$uneImagePlante){
	    $info = "<p> <h1>";
	    $info .= $nomPlante;
	    $info .= "</h1> <br> <div id ='image'>";
	    $info .= $uneImagePlante;
	    $info .= "</div><br><br><div id = 'text'>";
	    $info .= $descPlante;
	    $info .= "</div></p>";
	    echo $info;
	}

	public function creerInfoMaladie($nomMaladie,$conditionsFavorables,$uneImageMaladie,$periodeRisque,$symptomes,$stadeSensible){
	    $info = "<p> <h1>";
	    $info .= $nomMaladie;
	    $info .= "</h1> <br> <div id ='image'>";
	    $info .= $uneImageMaladie;
	    $info .= "</div><br><br><div id = 'text'> <h3> Conditions Favorables : </h3>";
	    $info .= $conditionsFavorables;
	    $info .= "<br> <h3> Periode de risque : </h3>";
	    $info .= $periodeRisque;
	    $info .= "<br> <h3> Symptomes : </h3>";
	    $info .= $symptomes;
	    $info .= "<br> <h3> Stade Sensible : </h3>";
	    $info .= $stadeSensible;
	    $info .= "<br>";
	    $info .= "</div></p>";
	    echo $info;
	}


	public function creerListeRavageurs($composantActif){
	    $menu = "<div class='container'>";
	    $menu .= "<div class='list-group'>";
	    $menu .= "<ul class = '" .  $this->style . "'>";
		foreach($this->composants as $composant){
		    $menu .= "<div class='row'>";
		    $menu .= " <div class='col-3'>";
			if($composant[0] == $composantActif){

				$menu .=  "<span class='list-group-item active'" . $composant[1] ."</span>";
			}
			else{

				$menu .= "<a href='index.php?menuRavageur";
				$menu .= "=" . $composant[0] . "' >";
				$menu .= "<span class='list-group-item'>" . $composant[1] ."</span>";
				$menu .= "</a>";
			}

			$menu .= "</div>";
			$menu .= "</div>";
		}
		$menu .= "</ul>";
		$menu .= "</div>";
		$menu .= "</div>";
		
	    echo $menu ;
	}

	public function creerInfoRavageur($nomRavageur,$stadeActifRavageur,$photoRavageur,$nombreGenerationsRavageur,$symptomesRavageur,$stadeSensibleRavageur,$periodeRisqueRavageur){
	    $info = "<p> <h1>";
	    $info .= $nomRavageur;
	    $info .= "</h1> <br> <div id ='image'>";
	    $info .= $photoRavageur;
	    $info .= "</div><br><br><div id = 'text'> <h3> Stade Actif : </h3>";
	    $info .= $stadeActifRavageur;
	     $info .= "</div><br><br><div id = 'text'> <h3> Nombre générations : </h3>";
	    $info .= $nombreGenerationsRavageur;
	    $info .= "<br> <h3> Periode de risque : </h3>";
	    $info .= $periodeRisqueRavageur;
	    $info .= "<br> <h3> Symptomes : </h3>";
	    $info .= $symptomesRavageur;
	    $info .= "<br> <h3> Stade Sensible : </h3>";
	    $info .= $stadeSensibleRavageur;
	    $info .= "<br>";
	    $info .= "</div></p>";
	    echo $info;
	}
	

}