<?php
class tableau {
	
	private $nbCol;
	private $nbLig;
	private $titreTab;
	private $titreCols;
	private $titreCol;
	private $donneesTab;
	private $idTab;


	public function __construct($unIdTab , $lesDonnees){
		$this->idTab = $unIdTab;
		$this->donneesTab = $lesDonnees;
	}
			
	public function setTitreTab($unTitreTab){
		$this->titreTab = $unTitreTab;
	}
	
	public function setTitreCols($lesTitreCols){
		$this->titreCols = $lesTitreCols;
	}
	
	public function setTitreCol($lesTitreCol){
		$this->titreCol = $lesTitreCol;
	}

	public function getDonneesTab(){
		return $this->donneesTab;
	}
	
	
	public function afficheTableau(){
		
		$tabHeadChaine ="";
		$tabChaine = "<table class='table' id = '". $this->idTab . "' >";
		
		//Afficher le titre du tableau
		if(!empty($this->titreTab)){
			$tabHeadChaine =  "<thead class='thead-dark'> <tr > <th scope='row' colspan = '";
			$tabHeadChaine .= count($this->donneesTab[0]);
			$tabHeadChaine .= "' >";
			$tabHeadChaine .= $this->titreTab;
			$tabHeadChaine .= "</th></tr></thead>";
		}
		
		
		//Afficher les entêtes des colonnes
		if(!empty($this->titreCol)){
			$tabHeadChaine .= "<thead class='thead-dark'><tr>";
			foreach($this->titreCol as $cellule){
				$tabHeadChaine .= "<th scope='row'>";
				$tabHeadChaine .= $cellule;
				$tabHeadChaine .= "</th>";
			}
			$tabHeadChaine .= "</tr></thead>";
		}
		
		//Afficher le corps du tableau
		$tabBodyChaine = "<tbody>";

		//var_dump($this->donneesTab);

		foreach($this->donneesTab as $ligne){
			$tabBodyChaine .=  "<tr>";
			//var_dump($ligne);
			//echo("</br>");
			$ligne = get_object_vars($ligne);	

			//var_dump($ligne);	

			foreach($ligne as $cellule){
				//echo $ligne->getConditionsFavorables();	
				//var_dump($cellule);
				$tabBodyChaine .=  "<td>";
				$tabBodyChaine .=  $cellule;
				$tabBodyChaine .=  "</td>";
			}
			$tabBodyChaine .=  "</tr>";
		}
		$tabBodyChaine .=  "</tbody></table>";
		
		$tabChaine .= $tabHeadChaine . $tabBodyChaine;
		
		echo $tabChaine;	
	}

	public function afficheTableauBioAgresseur(){
		$formMaladie = new Formulaire("post", "index.php?menuPlante=1", "Plantes", '');
		$tabHeadChaine ="";
		//$tabChaine = "<div class='col-md-6'>";
		$tabChaine = "<table class='table' id = '". $this->idTab . "' >";
		
		//Afficher le titre du tableau
		if(!empty($this->titreTab)){
			$tabHeadChaine =  "<thead class='thead-dark'> <tr > <th scope='row' colspan = '";
			$tabHeadChaine .= count($this->donneesTab[0]);
			$tabHeadChaine .= "' >";
			$tabHeadChaine .= $this->titreTab;
			$tabHeadChaine .= "</th></tr></thead>";
		}
		
		
		//Afficher les entêtes des colonnes
		if(!empty($this->titreCol)){
			$tabHeadChaine .= "<thead class='thead-dark'><tr>";
			foreach($this->titreCol as $cellule){
				$tabHeadChaine .= "<th scope='row'>";
				$tabHeadChaine .= $cellule;
				$tabHeadChaine .= "</th>";
			}
			$tabHeadChaine .= "</tr></thead>";
		}
		
		//Afficher le corps du tableau
		$tabBodyChaine = "<tbody>";

		//var_dump($this->donneesTab);

		foreach($this->donneesTab as $lignetab){
			$tabBodyChaine .=  "<tr>";
			//var_dump($ligne);
			//echo("</br>");

			$tabBodyChaine .=  "<td>";
			$tabBodyChaine .=  $lignetab->getNomBioAgresseur();
			$tabBodyChaine .=  "</td>";
			$tabBodyChaine .=  "<td>";
			$format = "suppr%d";
		    $suppr = sprintf($format,$lignetab->getIdBioAgresseur());
		   	$boutonSuppr = $formMaladie->creerInputSubmitTableau($suppr,$lignetab->getIdBioAgresseur(),'Supprimer');
			//$tabBodyChaine .=  "<button class='' type = 'submit' name = '" . $suppr . "' value = '" . $lignetab->getIdBioAgresseur() . "' onclick='".$_SESSION['IdBioAgresseur']."'=this.value;> Supprimer </button>";
			$tabBodyChaine .= "<input type='hidden' value='' id='ref_id_bouton'>";
			$formMaladie->ajouterComposantLigne($boutonSuppr, 1);
            $formMaladie->ajouterComposantTab();
            $formMaladie->creerFormulaire();
            $tabBodyChaine .= $formMaladie->getFormulaire();
			$tabBodyChaine .=  "</td>";
			
			$tabBodyChaine .=  "</tr>";
		}
		$tabBodyChaine .=  "</tbody></table>";
		//$tabChaine .= "</div>";
		
		$tabChaine .= $tabHeadChaine . $tabBodyChaine;
		
		echo $tabChaine;	
	}
	
}