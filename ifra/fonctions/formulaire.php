<?php
class Formulaire{
    private $method;
    private $action;
    private $nom;
    private $style;
    private $formulaireToPrint;
    private $erreur;

    private $ligneComposants = array();
    private $tabComposants = array();

    public function __construct($uneMethode, $uneAction , $unNom,$unStyle, $erreur = null ){
        $this->method = $uneMethode;
        $this->action =$uneAction;
        $this->nom = $unNom;
        $this->style = $unStyle;
        $this->erreur = $erreur;

    }


    public function ajouterComposantLigne($unComposant){
        $this->ligneComposants[] = $unComposant;
    }

    public function ajouterComposantTab(){
        $this->tabComposants[] = $this->ligneComposants;
        $this->ligneComposants = array();
    }


    public function creerLabelFor($unLabel){
        $composant = "";
        return $composant;
    }

    public function creerInputTexte($unNom, $unId, $unLabel, $uneValue , $required , $placeholder, $readonly){
        $composant = "<label for = '" . $unNom . "' />" . $unLabel . "</label>";
        $composant .= "<input class=\"form-control\" type = 'text' name = '" . $unNom . "' id = '" . $unId . "' ";
        if (!empty($uneValue)){
            $composant .= "value = '" . $uneValue . "' ";
        }
        if (!empty($placeholder)){
            $composant .= "placeholder = '" . $placeholder . "' ";
        }
        if ( $required == 1){
            $composant .= "required";
        }

        if( $readonly == 1){
            $composant .= " readonly";
        }
        $composant .= "/>";
        return $composant;
    }

    public function creerInputTextArea($unNom, $nbLigne, $nbCol,$value,$unLabel){
        $composant = "<label for = '" . $unNom . "' />" . $unLabel . "</label></br>";
        $composant .= "<textarea name = '" . $unNom . "' rows='".$nbLigne."'  cols='".$nbCol."'";

        $composant .= "/>".$value."</textarea>";
        return $composant;
    }

    public function creerInputHidden($unNom, $unId, $uneValue){
        $composant = "<input id='".$unId."' name='".$unNom."' type='hidden' value='".$uneValue."'>";
        return $composant;
    }

    public function creerInputTexteMaxLength($unNom, $unId, $unLabel, $uneValue , $required , $placeholder, $readonly, $maxlength){
        $composant = "<label for = '" . $unNom . "' />" . $unLabel . "</label>";
        $composant .= "<input class=\"form-control\" type = 'text' name = '" . $unNom . "' id = '" . $unId . "' ";
        if (!empty($uneValue)){
            $composant .= "value = '" . $uneValue . "' ";
        }
        if (!empty($maxlength)){
            $composant .= "maxlength = '" . $maxlength . "' ";
        }
        if (!empty($placeholder)){
            $composant .= "placeholder = '" . $placeholder . "' ";
        }
        if ( $required == 1){
            $composant .= "required";
        }

        if( $readonly == 1){
            $composant .= " readonly";
        }
        $composant .= "/>";
        return $composant;
    }

    public function creerInputMdp($unNom, $unId, $unLabel, $uneValue , $required , $placeholder, $readonly){
        $composant = "<label for = '" . $unNom . "' />" . $unLabel . "</label>";
        $composant .= "<input class=\"form-control\" type = 'password' name = '" . $unNom . "' id = '" . $unId . "' ";
        if (!empty($uneValue)){
            $composant .= "value = '" . $uneValue . "' ";
        }
        if (!empty($placeholder)){
            $composant .= "placeholder = '" . $placeholder . "' ";
        }
        if ( $required == 1){
            $composant .= "required";
        }

        if( $readonly == 1){
            $composant .= " readonly";
        }
        $composant .= "/>";
        return $composant;
    }

    public function creerSelect($unNom, $unId, $unLabel, $options){
        $composant = "<label for = '" . $unNom . "' />" . $unLabel . "</label>";
        $composant .= "<select  class=\"form-control\" name = '" . $unNom . "' id = '" . $unId . "' >";
        foreach ($options as $option){
            $composant .= "<option value = ".$option.">".$option."</option>";
        }
        $composant .= "</select>";
        return $composant;
    }

    public function creerSelectCodeType($unNom, $unId, $unLabel, $options){
        $composant = "<label for = '" . $unNom . "' />" . $unLabel . "</label>";
        $composant .= "<select  class=\"form-control\" name = '" . $unNom . "' id = '" . $unId . "' >";
        foreach ($options as $option){
            $composant .= "<option value = ".$option["codeType"].">".$option["codeType"]."</option>";
        }
        $composant .= "</select>";
        return $composant;
    }

    public function creerInputSubmit($unNom, $unId, $uneValue, $class){
        $composant = "<button class='".$class."' type = 'submit' name = '" . $unNom . "' id = '" . $unId . "' style='cursor: pointer;'/>";
        $composant .= $uneValue."</button> ";
        return $composant;
    }
	 public function creerInputSubmitBis($unNom, $unId, $uneValue){
        $composant = "<button type = 'submit' name = '" . $unNom . "' id = '" . $unId . "' style='cursor: pointer;'> Supprimer </button>/>";
        $composant .= $uneValue."</button> ";
        return $composant;
    }

     public function creerInputSubmitTableau($unNom, $unId, $uneValue){
        $composant = "<button class='btn btn-outline-secondary' type = 'submit' name = '" . $unNom . "' id = '" . $unId . "' style='cursor: pointer; onclick='".$_SESSION['IdBioAgresseur']."'=this.id;'/>";
        $composant .= $uneValue."</button> ";
        return $composant;
    }

    public function creerInputImage($unNom, $unId, $uneSource){
        $composant = "<input type = 'image' name = '" . $unNom . "' id = '" . $unId . "' ";
        $composant .= "src = '" . $uneSource . "'/> ";
        return $composant;
    }


    public function creerInputDate($unNom, $unId, $uneValue)
    {
        $composant = "<input type = 'date' name = '" . $unNom . "' id = '" . $unId . "' value = '" . $uneValue . "' />";
        return $composant;
    }

    public function creerInputNumber($unNom, $unId, $uneValue, $placeholder, $step = 1)
    {
        $composant = "<input class=\"form-control\" type = 'number' name = '" . $unNom . "' id = '" . $unId . "' value = '" . $uneValue . "' placeholder = '" . $placeholder . "' id = '" . $step . "'/>";
        return $composant;
    }

    //Permet de créer un input file
    public function creerInputFile($unNom, $unId, $uneSource){
        $composant = "<input type = 'file' name = '" . $unNom . "' id = '" . $unId . "' ";
        $composant .= "src = '" . $uneSource . "'/> ";
        return $composant;
    }


    public function creerFormulaire(){

        $this->formulaireToPrint = "<div class=\"container\"><form method = '" .  $this->method . "' ";
        $this->formulaireToPrint .= "action = '" .  $this->action . "' ";
        $this->formulaireToPrint .= "name = '" .  $this->nom . "' ";
        $this->formulaireToPrint .= "class = '" .  $this->style . "' ><div class=\"form-group\">";
        $this->formulaireToPrint .= "<div class = \"alert alert-danger\" >".$this->erreur."</div>";

        $this->formulaireToPrint .= $this->erreur;

        foreach ($this->tabComposants as $uneLigneComposants){
            foreach ($uneLigneComposants as $unComposant){
                $this->formulaireToPrint .= $unComposant ;
            }
        }
        $this->formulaireToPrint .= "</div></form></div>";
        return $this->formulaireToPrint ;
    }

    public function afficherFormulaire(){
        echo $this->formulaireToPrint ;
    }

    public function getFormulaire(){
        return $this->formulaireToPrint ;
    }

}