<?php

require_once 'modeles/dao/planteDAO.php';

$_SESSION['listePlantes'] = new Plantes(PlanteDAO::lesPlantesBioAgresseurs());

if(!isset($_SESSION['IdBioAgresseur'])){
		$_SESSION['IdBioAgresseur'] = "";
}

if(isset($_GET['menuPlante'])){
	$_SESSION['menuPlante']= $_GET['menuPlante'];
}
else
{
	if(!isset($_SESSION['menuPlante'])){
		$_SESSION['menuPlante']="1";
	}
}

$menuPlante = new menu("menuPlante","menuPlante");
foreach ($_SESSION['listePlantes']->getPlantes() as $unePlante) {
	$menuPlante->ajouterComposant($menuPlante->creerItemLien($unePlante->getIdPlante(),$unePlante->getNomPlante()));
	//var_dump($_SESSION['listePlantes']);
}


$planteActive = $_SESSION['listePlantes']->cherchePlante($_SESSION['menuPlante']);
//var_dump($planteActive);
//var_dump($_SESSION['listePlantes']);
//var_dump($_SESSION['menuPlante']);
//var_dump(PlanteDAO::lesPlantesBioAgresseurs());

$nomPlante = $planteActive->getNomPlante();
$photoPlante = "<img src= 'images/".lcfirst($planteActive->getNomPlante()).".jpg'/>";
$descriptifPlante = $planteActive->getDescriptifPlante();

$infoPlante = new menu("infoPlante","infoPlante");


$listeMaladies = $planteActive->getLesMaladies();
$listeAllMaladies = maladieDAO::lesMaladies();
$listeAllRavageurs = ravageurDAO::lesRavageurs();
//$listeMaladies = lesMaladiesPlante($_SESSION['menuPlante']);


//var_dump($_SESSION['IdBioAgresseur']);


//$tabMaladies = new tableau('tabMaladies',$listeMaladies);
//$tabMaladies->setTitreTab('Maladies');

$affichageMaladie = "</br> <div class='row'> <div class='col-5'><table class='table'>
			<thead class='thead-dark'>
			<th scope='col'> Maladies </th>";
			if($_SESSION['typeUtilisateur'] == 'SLR'){
			$affichageMaladie .="<th scope='col'> </th>";
			$affichageMaladie .= "</thead>";
		}
//var_dump($listeMaladies);
foreach($listeMaladies as $maladie)
{
	$affichageMaladie .= "<tr>";
	$affichageMaladie .= "<td>";
	$affichageMaladie .= $maladie->getNomBioAgresseur();
	$affichageMaladie .= "</td>";
	if($_SESSION['typeUtilisateur'] == 'SLR'){
	$affichageMaladie .= "<td>";

	$affichageMaladie .= "<a class='btn btn-light' href='index.php?menuPlante=".$_SESSION['menuPlante']."&amp;supprimer=1&amp;nummaladie=" .$maladie->getIdBioAgresseur(). "'' type = 'submit' style='cursor: pointer;'/>Supprimer</a>";

	$affichageMaladie .= "</td>";
	}
	$affichageMaladie .= "</tr>";
}
	
	$affichageMaladie .= "</table> </div>";
		if($_SESSION['typeUtilisateur'] == 'SLR'){
	$affichageMaladie .= "<div class='col'>";
	$affichageMaladie .= "<form action='' method='get'>";
	$affichageMaladie .= "<select class='form-control' name='selectMaladie'>";

	foreach ($listeAllMaladies as $maladie) {

	$nomMaladie = $maladie->getNomBioAgresseur();
	$numMaladie = $maladie->getIdBioAgresseur();
 	$affichageMaladie .= "<option value='".$numMaladie."'>".$nomMaladie."</option> ";

	}
	
	$affichageMaladie .= "</select>";
	//$affichageMaladie .= "<a class='btn btn-light' href='index.php?biorelaiMP=".$_SESSION['menuPlante']."&amp;ajouter=1&amp;selectMaladie=".$_GET['selectMaladie']."' type = 'submit' style='cursor: pointer;'/>Ajouter une maladie</a>";
	$affichageMaladie .= "<button type='submit' name='btnListe' >Ajouter une maladie</button>";
	$affichageMaladie .= "</form>";
	$affichageMaladie .= "</div>";
}

if(isset($_GET['nummaladie']) && isset($_GET['supprimer'])){
	//var_dump($_GET['nummaladie']);
	//var_dump($_GET['supprimer']);
	MaladieDAO::supprimerMaladie($_GET['nummaladie'],$_GET['menuPlante']);
	//sleep(2);
	header("Location: index.php?menuPlante=".$_GET['menuPlante']."");

}

if(isset($_GET['selectMaladie'])){
	$laMaladie = $_GET['selectMaladie'];
	planteDAO::planteSensible($_SESSION['menuPlante'],$laMaladie);
	header("Location:index.php?ifraMP=plantes");
}


//$tabMaladies->afficheTableau();
//var_dump($tabMaladies->getDonneesTab());

//var_dump($tabMaladies->getDonneesTab());
//var_dump($listeMaladies);

$affichageRavageur = " <div class='col-4'><table class='table'>";
			$affichageRavageur .= "<thead class='thead-dark'>";
			$affichageRavageur .= "<th scope='col'> Ravageurs </th>";
			if($_SESSION['typeUtilisateur'] == 'SLR'){
			$affichageRavageur .= "<th scope='col'> </th>";
			}	
			$affichageRavageur .= "</thead>";
			

$listeRavageurs = $planteActive->getLesRavageurs();

foreach($listeRavageurs as $ravageur)
{
	$affichageRavageur .= "<tr>";
	$affichageRavageur .= "<td>";
	$affichageRavageur .= $ravageur->getNomBioAgresseur();
	$affichageRavageur .= "</td>";
	if($_SESSION['typeUtilisateur'] == 'SLR'){
	$affichageRavageur .= "<td>";
	$affichageRavageur .= "<a class='btn btn-light' href='index.php?menuPlante=".$_SESSION['menuPlante']."&amp;supprimer=1&amp;numravageur=" .$ravageur->getIdBioAgresseur(). "'' type = 'submit' style='cursor: pointer;'/>Supprimer</a>";
	$affichageRavageur .= "</td>";
}
	$affichageRavageur .= "</tr>";
}

$affichageRavageur .= "</table></div>";
	if($_SESSION['typeUtilisateur'] == 'SLR'){
$affichageRavageur .= "<div class='col'>";
$affichageRavageur .= "<form action='' method='get'>";
	$affichageRavageur .= "<select class='form-control' name='selectRavageur'>";
	//var_dump($listeAllRavageurs);
	foreach ($listeAllRavageurs as $ravageur) {

	$nomRavageur = $ravageur->getNomBioAgresseur();
	$numRavageur = $ravageur->getIdBioAgresseur();
 	$affichageRavageur .= "<option value='".$numRavageur."'>".$nomRavageur."</option> ";

	}
	
	$affichageRavageur .= "</select>";
	//$affichageMaladie .= "<a class='btn btn-light' href='index.php?biorelaiMP=".$_SESSION['menuPlante']."&amp;ajouter=1&amp;selectMaladie=".$_GET['selectMaladie']."' type = 'submit' style='cursor: pointer;'/>Ajouter une maladie</a>";
	$affichageRavageur .= "<button type='submit' name='btnListeRava' >Ajouter un ravageur</button>";
	$affichageRavageur .= "</form>";
	$affichageRavageur .= "</div>";

//$affichageRavageur .= "<a class='btn btn-light' href='index.php?biorelaiMP=".$_SESSION['menuPlante']."&amp;ajouterPlante=1' type = 'submit' style='cursor: pointer;'/>Ajouter un ravageur</a></div></div></br>";

$affichageRavageur .= "<a class='btn btn-light' href='index.php?menuPlante=".$_SESSION['menuPlante']."&amp;ajouterPlante=1' type = 'submit' style='cursor: pointer;'/>Ajouter une plante</a>";
$affichageRavageur .= "<a class='btn btn-light' href='index.php?biorelaiMP=".$_SESSION['menuPlante']."&amp;modifierPlante=1'' type = 'submit' style='cursor: pointer;'/>Modifier cette plante</a>";
$affichageRavageur .= "<a class='btn btn-light' href='index.php?biorelaiMP=".$_SESSION['menuPlante']."&amp;supprimerPlante=1'  type = 'submit' style='cursor: pointer;'/>Supprimer cette plante</a>";
}

if(isset($_GET['numravageur']) && isset($_GET['supprimer'])){
	//var_dump($_GET['nummaladie']);
	//var_dump($_GET['supprimer']);
	MaladieDAO::supprimerMaladie($_GET['numravageur'],$_GET['menuPlante']);
	//sleep(5);
	header("Location:index.php?ifraMP=plantes");

}

if(isset($_GET['selectRavageur'])){
	$leRavageur = $_GET['selectRavageur'];
	planteDAO::planteSensible($_SESSION['menuPlante'],$leRavageur);
	header("Location:index.php?ifraMP=plantes");
}
//var_dump($listeRavageurs);

//$tabRavageurs = new tableau('tabRavageurs',$listeRavageurs);
//$tabRavageurs->setTitreTab('Ravageurs');
if(isset($_GET['supprimerPlante'])){
	PlanteDAO::supprimerPlante($_SESSION['menuPlante']);
	header("Location: index.php?menuPlante=1");
}

if(isset($_GET['ajouterPlante'])){
	header("Location: index.php?ifraMP=ajoutPlante");
}

if(isset($_GET['modifierPlante'])){
	header("Location: index.php?ifraMP=modifierPlante");
}


require_once 'vue/vuePlantes.php' ;
