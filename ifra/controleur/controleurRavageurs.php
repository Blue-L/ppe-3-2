<?php

require_once 'modeles/dao/ravageurDAO.php';

$_SESSION['listeRavageurs'] = RavageurDAO::lesRavageurs();
$_SESSION['listeObjRavageurs'] = new Ravageurs(RavageurDAO::lesRavageurs());

if(!isset($_SESSION['IdBioAgresseur'])){
		$_SESSION['IdBioAgresseur'] = "";
}

if(isset($_GET['menuRavageur'])){
	$_SESSION['menuRavageur']= $_GET['menuRavageur'];
}
else
{
	if(!isset($_SESSION['menuRavageur'])){
		$_SESSION['menuRavageur']="2";
	}
}

$menuRavageur = new menu("menuRavageur","menuRavageur");
//var_dump($_SESSION['listeRavageurs']);
foreach ($_SESSION['listeRavageurs'] as $unRavageur) {
	$menuRavageur->ajouterComposant($menuRavageur->creerItemLien($unRavageur->getIdBioAgresseur(),$unRavageur->getNomBioAgresseur()));
}

//var_dump($_SESSION['menuRavageur']);
$RavageurActive = $_SESSION['listeObjRavageurs']->chercheRavageur($_SESSION['menuRavageur']);
//var_dump($RavageurActive);
//var_dump($RavageurActive);
$nomRavageur = $RavageurActive->getNomBioAgresseur();
$photoRavageur = "<img src= 'images/".lcfirst($RavageurActive->getNomBioAgresseur()).".jpg'/>";
$stadeActifRavageur = $RavageurActive->getStadeActif();
$nombreGenerationsRavageur = $RavageurActive->getNombreGenerations();
$periodeRisqueRavageur = $RavageurActive->getPeriodeRisqueBioAgresseur();
$symptomesRavageur = $RavageurActive->getSymptomesBioAgresseur();
$stadeSensibleRavageur = $RavageurActive->getStadeSensibleBioAgresseur();

$infoRavageur = new menu("infoRavageur","infoRavageur");


$affichage = "<a class='btn btn-light' href='index.php?menuRavageur=".$_SESSION['menuRavageur']."&amp;ajouterRavageur=1' type = 'submit' style='cursor: pointer;'/>Ajouter un Ravageur</a>";
$affichage .= "<a class='btn btn-light' href='index.php?biorelaiMP=".$_SESSION['menuRavageur']."&amp;modifierRavageur=1'' type = 'submit' style='cursor: pointer;'/>Modifier ce Ravageur</a>";
$affichage .= "<a class='btn btn-light' href='index.php?biorelaiMP=".$_SESSION['menuRavageur']."&amp;supprimerRavageur=1'  type = 'submit' style='cursor: pointer;'/>Supprimer ce Ravageur</a>";

if(isset($_GET['supprimerRavageur'])){
	RavageurDAO::supprimerUnRavageur($_SESSION['menuRavageur']);
	header("Location: index.php?menuRavageur=2");
}

if(isset($_GET['ajouterRavageur'])){
	header("Location: index.php?ifraMP=ajoutRavageur");
}

if(isset($_GET['modifierRavageur'])){
	header("Location: index.php?ifraMP=modifierRavageur");
}




require_once 'vue/vueRavageurs.php' ;