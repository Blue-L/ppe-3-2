<?php

$formPlante = new Formulaire("post", "index.php", "Plante", 'Plante', $messageErreurConn);

$unComposant = $formPlante->creerInputTexteMaxLength('nomPlante', 'nomPlante', 'Nom :',NULL , 1, 'Saisissez le nom de la plante', 0,128);
$formPlante->ajouterComposantLigne($unComposant, 1);
$formPlante->ajouterComposantTab();



$unComposant = $formPlante->creerInputTexte('descriptifPlante', 'descriptifPlante', 'Description :',NULL, 1, 'Saisissez la description de la plante', 0);
$formPlante->ajouterComposantLigne($unComposant, 1);
$formPlante->ajouterComposantTab();



$unComposant = $formPlante->creerInputSubmit('validerInfoPlante', 'validerInfoPlante', 'Valider', 'btn btn-primary mt-2 w-100');
$formPlante->ajouterComposantLigne($unComposant, 1);
$formPlante->ajouterComposantTab();


$formPlante->creerFormulaire();

require_once 'vue/ajoutPlante.php' ;