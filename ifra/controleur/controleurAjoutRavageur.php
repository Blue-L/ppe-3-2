<?php

$formRavageur = new Formulaire("post", "index.php", "Ravageur", 'Ravageur', $messageErreurConn);

$unComposant = $formRavageur->creerInputTexteMaxLength('nomRavageur', 'nomRavageur', 'Nom :',NULL , 1, 'Saisissez le nom du ravageur', 0,40);
$formRavageur->ajouterComposantLigne($unComposant, 1);
$formRavageur->ajouterComposantTab();

$unComposant = $formRavageur->creerInputTexteMaxLength('stadeActifRavageur', 'stadeActifRavageur', 'Stade actif :',NULL , 1, 'Saisissez le stade actif', 0,40);
$formRavageur->ajouterComposantLigne($unComposant, 1);
$formRavageur->ajouterComposantTab();

$unComposant = $formRavageur->creerInputTexteMaxLength('nombreGenerationsRavageur', 'nombreGenerationsRavageur', 'Nombre generations :',NULL , 1, 'Saisissez le nombre de generations', 0,40);
$formRavageur->ajouterComposantLigne($unComposant, 1);
$formRavageur->ajouterComposantTab();

$unComposant = $formRavageur->creerInputTexteMaxLength('periodeRisqueRavageur', 'periodeRisqueRavageur', 'Periode Risque :',NULL , 1, 'Saisissez la periode de risque', 0,300);
$formRavageur->ajouterComposantLigne($unComposant, 1);
$formRavageur->ajouterComposantTab();



$unComposant = $formRavageur->creerInputTextArea('symptomesBioAgresseur', '10', '155 :',NULL,'Symptomes :');
$formRavageur->ajouterComposantLigne($unComposant, 1);
$formRavageur->ajouterComposantTab();

$unComposant = $formRavageur->creerInputTexteMaxLength('stadeSensibleRavageur', 'stadeSensibleRavageur', 'Stade sensible de la Ravageur:',NULL , 1, 'Saisissez le stade sensible', 0,300);
$formRavageur->ajouterComposantLigne($unComposant, 1);
$formRavageur->ajouterComposantTab();

$unComposant = $formRavageur->creerInputSubmit('validerNewRavageur', 'validerNewRavageur', 'Valider', 'btn btn-primary mt-2 w-100');
$formRavageur->ajouterComposantLigne($unComposant, 1);
$formRavageur->ajouterComposantTab();


$formRavageur->creerFormulaire();

require_once 'vue/ajoutRavageur.php' ;