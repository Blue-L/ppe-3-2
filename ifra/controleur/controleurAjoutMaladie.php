<?php

$formMaladie = new Formulaire("post", "index.php", "Maladie", 'Maladie', $messageErreurConn);

$unComposant = $formMaladie->creerInputTexteMaxLength('nomMaladie', 'nomMaladie', 'Nom :',NULL , 1, 'Saisissez le nom de la maladie', 0,40);
$formMaladie->ajouterComposantLigne($unComposant, 1);
$formMaladie->ajouterComposantTab();

$unComposant = $formMaladie->creerInputTexteMaxLength('conditionsFavorablesMaladie', 'conditionsFavorablesMaladie', 'Conditions Favorables :',NULL , 1, 'Saisissez les conditions favorables', 0,40);
$formMaladie->ajouterComposantLigne($unComposant, 1);
$formMaladie->ajouterComposantTab();

$unComposant = $formMaladie->creerInputTexteMaxLength('periodeRisqueMaladie', 'periodeRisqueMaladie', 'Periode Risque :',NULL , 1, 'Saisissez la periode de risque', 0,300);
$formMaladie->ajouterComposantLigne($unComposant, 1);
$formMaladie->ajouterComposantTab();



$unComposant = $formMaladie->creerInputTextArea('symptomesBioAgresseur', '10', '155 :',NULL,'Symptomes :');
$formMaladie->ajouterComposantLigne($unComposant, 1);
$formMaladie->ajouterComposantTab();

$unComposant = $formMaladie->creerInputTexteMaxLength('stadeSensibleMaladie', 'stadeSensibleMaladie', 'Stade sensible de la maladie:',NULL , 1, 'Saisissez le stade sensible', 0,300);
$formMaladie->ajouterComposantLigne($unComposant, 1);
$formMaladie->ajouterComposantTab();

$unComposant = $formMaladie->creerInputSubmit('validerNewMaladie', 'validerNewMaladie', 'Valider', 'btn btn-primary mt-2 w-100');
$formMaladie->ajouterComposantLigne($unComposant, 1);
$formMaladie->ajouterComposantTab();


$formMaladie->creerFormulaire();

require_once 'vue/ajoutMaladie.php' ;