<?php

if(!isset($_SESSION['identification']) || !$_SESSION['identification'])
{
    

$formConn = new Formulaire("post", "index.php", "Connexion", 'Connexion', $messageErreurConn);

$unComposant = $formConn->creerInputTexte('login', 'login', 'Identifiant :','' , 1, 'Saisissez votre identifiant', 0);
$formConn->ajouterComposantLigne($unComposant, 1);
$formConn->ajouterComposantTab();

$unComposant = $formConn->creerInputMdp('pass', 'pass', 'Mot de passe :', '', 1, 'Saisissez votre mdp', 0);
$formConn->ajouterComposantLigne($unComposant, 1);
$formConn->ajouterComposantTab();

$unComposant = $formConn->creerInputSubmit('seConnecter', 'seConnecter', 'Login', 'btn btn-primary mt-2 w-100');
$formConn->ajouterComposantLigne($unComposant, 1);
$formConn->ajouterComposantTab();

$formConn->creerFormulaire();
}

else {
    $_SESSION['identification'] = [];
    $_SESSION['numUtilisateur'] = NULL;
    $_SESSION['typeUtilisateur'] = NULL;
    $_SESSION['ifraMP']="accueil";
    header('location:index.php');
}

require_once 'vue/connexion.php' ;