<?php
require_once 'fonctions/formulaire.php';
require_once 'fonctions/tableau.php';
require_once 'modeles/dao/utilisateurDAO.php';
require_once 'modeles/dto/Utilisateurs.php';
require_once 'modeles/dto/Utilisateur.php';
require_once 'modeles/dao/codeTypeDAO.php';

$_SESSION['listeUtilisateurs']=new Utilisateurs(utilisateurDAO::recupUtilisateurs());
if(isset($_POST['Ajouter']) ){
	$_SESSION['Ajouter']='Ajouter';
	$formulaireNouveauCompte = new Formulaire('post','index.php','formuComptes','formuComptes');
	$composant=$formulaireNouveauCompte->creerInputTexte('idUtilisateur', 'idUtilisateur', 'idUtilisateur', 'idUtilisateur', 1 , 'identifiant', 0);
	$formulaireNouveauCompte->ajouterComposantLigne($composant);
	$formulaireNouveauCompte->ajouterComposantTab();

	//Combo box contenant tout les types d'utilisateurs autre que le directeur
	/*
	$leCodeType = [];

	foreach (codeTypeDAO::recupCodeType() as $codeType ) {
		$leCodeType[] = $codeType; 
	}*/
	//var_dump(codeTypeDAO::recupCodeType()); 
	
	$composant = $formulaireNouveauCompte->creerSelectCodeType('codeType', 'codeType', "Type de l'utilisateur :",codeTypeDAO::recupCodeType()) ;
	
	$formulaireNouveauCompte->ajouterComposantLigne($composant);
	$formulaireNouveauCompte->ajouterComposantTab();
	$composant=$formulaireNouveauCompte->creerInputTexte('loginUtilisateur', 'loginUtilisateur', 'loginUtilisateur', 'loginUtilisateur', 1 , 'login', 0);
	$formulaireNouveauCompte->ajouterComposantLigne($composant);
	$formulaireNouveauCompte->ajouterComposantTab();
	$composant=$formulaireNouveauCompte->creerInputTexte('mdpUtilisateur', 'mdpUtilisateur', 'mdpUtilisateur', 'mdpUtilisateur', 1 , 'mot de passe', 0);
	$formulaireNouveauCompte->ajouterComposantLigne($composant);
	$formulaireNouveauCompte->ajouterComposantTab();
	$composant=$formulaireNouveauCompte->creerInputTexte('mdpUtilisateurVerif', 'mdpUtilisateurVerif', 'mdpUtilisateurVerif', 'mdpUtilisateurVerif', 1 , 'mot de passe verification', 0);
	$formulaireNouveauCompte->ajouterComposantLigne($composant);
	$formulaireNouveauCompte->ajouterComposantTab();

	$composant = $formulaireNouveauCompte->creerInputSubmitBis('Valider','Valider','Valider');
	$formulaireNouveauCompte->ajouterComposantLigne($composant);

	$composant = $formulaireNouveauCompte->creerInputSubmitBis('Retour','Retour','Retour');
	$formulaireNouveauCompte->ajouterComposantLigne($composant);
	$formulaireNouveauCompte->ajouterComposantTab();
	$formulaireNouveauCompte->creerFormulaire();
	

	if(isset($_POST['Retour'])){
		$_SESSION['Ajouter']= NULL;
	}


}else if (isset($_POST['Valider'])){

	if(!empty($_POST['idUtilisateur']) && !empty($_POST['mdpUtilisateur']) && !empty($_POST['mdpUtilisateurVerif']) && !empty( $_POST['loginUtilisateur']) && ($_POST['mdpUtilisateur'] == $_POST['mdpUtilisateurVerif'])){
			utilisateurDAO::insertUtilisateur($_POST['idUtilisateur'],$_POST['codeType'],$_POST['loginUtilisateur'],md5($_POST['mdpUtilisateur']));
		}else{
			echo ("L'un des champs n'est pas renseigné ou les champs mot de passe et mot de passe Verifications dont differents");
			$_POST['Valider'] = $_POST['Ajouter'];
			header('location:index.php');
		}
}
else{
	$tabUtilisateurs=array();
	$i=0;
	foreach ($_SESSION['listeUtilisateurs']->getUtilisateurs() as $unUtilisateur) {
	
		$tabUtilisateurs[$i][0]=$unUtilisateur->getCodeType();
		$tabUtilisateurs[$i][1]=$unUtilisateur->getLogin();
		$tabUtilisateurs[$i][2]=$unUtilisateur->getCodeType();
		$tabUtilisateurs[$i][3]= "<input type='submit' id ='deleteUser' value='Supprimer' name='deleteUser'/>"."<input type='hidden' id ='idUser' value= '".$unUtilisateur->getIdUtilisateur()."' />";
	$i++;
	}
	$formulaire = new Formulaire('post','index.php','formuComptes','formuComptes');
	$composant = $formulaire->creerInputSubmitBis('Ajouter','Ajouter','Ajouter');
	$formulaire->ajouterComposantLigne($composant);
	$formulaire->ajouterComposantTab();
	$formulaire->creerFormulaire();

	$tabUtilisateurs=new Tableau("tabUtilisateurs",$tabUtilisateurs);
	$tabUtilisateurs->setTitreTab('Utilisateurs');
	$tabUtilisateurs->setTitreCol(array('Login','Adresse Mail','Statut',"Supprimer l'utilisateur"));
	$_SESSION['Ajouter']= NULL;
	if($_POST['Supprimer']){
		utilisateurDAO::deleteUtilisateur($_POST['idUser']);
		header('location:index.php');
	}
}


require_once 'vue/vueComptes.php' ;