<?php

if(isset($_GET['ifraMP'])){
	$_SESSION['ifraMP']= $_GET['ifraMP'];
}
else
{
	if(!isset($_SESSION['ifraMP'])){
		$_SESSION['ifraMP']="accueil";
	}
}



$messageErreurConn = "";
if(isset($_POST['login'] , $_POST['pass']))
{
	$login = $_POST['login'];
	$pass = $_POST['pass'];
	$_SESSION['numUtilisateur'] = utilisateurDAO::getNumUtilisateur($login, $pass);

    $_SESSION['identification'] = utilisateurDAO::identification($login, $pass);
    if($_SESSION['identification'])
    {
        $_SESSION['ifraMP'] = "accueil";
		$_SESSION['typeUtilisateur'] = utilisateurDAO::getTypeUtilisateur($_SESSION['numUtilisateur']);
    }
    else 
    {
    	$_SESSION['typeUtilisateur'] = '';
        $messageErreurConn = "Login ou mot de passe incorrect";
    }

}
if(!isset($_SESSION['identification'])){
		$_SESSION['typeUtilisateur'] = '';
}

$ifraMP = new Menu("ifraMP");

$ifraMP->ajouterComposant($ifraMP->creerItemLien("accueil", "Accueil"));
$ifraMP->ajouterComposant($ifraMP->creerItemLien("contact", "Contact"));
$ifraMP->ajouterComposant($ifraMP->creerItemLien("plantes", "Plantes"));
$ifraMP->ajouterComposant($ifraMP->creerItemLien("maladies", "Maladies"));
$ifraMP->ajouterComposant($ifraMP->creerItemLien("ravageurs", "Ravageurs"));
$ifraMP->ajouterComposant($ifraMP->creerItemLien("observations", "Observations"));

if($_SESSION['typeUtilisateur'] == 'DIR'){
	$ifraMP->ajouterComposant($ifraMP->creerItemLien("comptes", "Comptes"));
}
$ifraMP->ajouterComposant($ifraMP->creerItemLien("compte", "Compte"));
if(isset($_SESSION['identification']) && $_SESSION['identification'])
{
	
	$ifraMP->ajouterComposant($ifraMP->creerItemLien("connexion", "Deconnexion"));
}
else
{
	$ifraMP->ajouterComposant($ifraMP->creerItemLien("connexion", "Connexion"));
}

if(isset($_POST['validerInfoPlante'])){
	planteDAO::ajouterPlante($_POST['nomPlante'],$_POST['descriptifPlante']);
	header("Location: index.php?ifraMP=plantes");
}

if(isset($_POST['validerModifPlante'])){
	$descriptionPlante = addslashes($_POST['descriptifPlante']);
	planteDAO::modifierPlante($_POST['idPlante'],$_POST['nomPlante'],$descriptionPlante);
	header("Location: index.php?ifraMP=plantes");
}

if(isset($_POST['nomMaladie'])){
	MaladieDAO::ajouterMaladie($_POST['nomMaladie'],$_POST['conditionsFavorablesMaladie'],$_POST['periodeRisqueMaladie'],$_POST['symptomesBioAgresseur'],$_POST['stadeSensibleMaladie']);
	header("Location: index.php?ifraMP=maladies");
}

if(isset($_POST['nomRavageur'])){
	RavageurDAO::ajouterRavageur($_POST['nomRavageur'],$_POST['stadeActifRavageur'],$_POST['nombreGenerationsRavageur'],$_POST['periodeRisqueRavageur'],$_POST['symptomesBioAgresseur'],$_POST['stadeSensibleRavageur']);
	header("Location: index.php?ifraMP=ravageurs");
}

$menuPrincipal = $ifraMP->creerMenu($_SESSION['ifraMP'],'ifraMP');




include dispatcher::dispatch($_SESSION['ifraMP']);
