<?php

require_once 'modeles/dao/maladieDAO.php';

$_SESSION['listeMaladies'] = maladieDAO::lesMaladies();
$_SESSION['listeObjMaladies'] = new Maladies(maladieDAO::lesMaladies());

if(!isset($_SESSION['IdBioAgresseur'])){
		$_SESSION['IdBioAgresseur'] = "";
}

if(isset($_GET['menuMaladie'])){
	$_SESSION['menuMaladie']= $_GET['menuMaladie'];
}
else
{
	if(!isset($_SESSION['menuMaladie'])){
		$_SESSION['menuMaladie']="1";
	}
}

$menuMaladie = new menu("menuMaladie","menuMaladie");
foreach ($_SESSION['listeMaladies'] as $uneMaladie) {
	$menuMaladie->ajouterComposant($menuMaladie->creerItemLien($uneMaladie->getIdBioAgresseur(),$uneMaladie->getNomBioAgresseur()));
}

$maladieActive = $_SESSION['listeObjMaladies']->chercheMaladie($_SESSION['menuMaladie']);
//var_dump($maladieActive);
$nomMaladie = $maladieActive->getNomBioAgresseur();
$photoMaladie = "<img src= 'images/".lcfirst($maladieActive->getNomBioAgresseur()).".jpg'/>";
$conditionsFavorablesMaladie = $maladieActive->getConditionsFavorables();
$periodeRisqueMaladie = $maladieActive->getPeriodeRisqueBioAgresseur();
$symptomesMaladie = $maladieActive->getSymptomesBioAgresseur();
$stadeSensibleMaladie = $maladieActive->getStadeSensibleBioAgresseur();

$infoMaladie = new menu("infoMaladie","infoMaladie");


$affichage = "<a class='btn btn-light' href='index.php?menuMaladie=".$_SESSION['menuMaladie']."&amp;ajouterMaladie=1' type = 'submit' style='cursor: pointer;'/>Ajouter une Maladie</a>";
$affichage .= "<a class='btn btn-light' href='index.php?biorelaiMP=".$_SESSION['menuMaladie']."&amp;modifierMaladie=1'' type = 'submit' style='cursor: pointer;'/>Modifier cette Maladie</a>";
$affichage .= "<a class='btn btn-light' href='index.php?biorelaiMP=".$_SESSION['menuMaladie']."&amp;supprimerMaladie=1'  type = 'submit' style='cursor: pointer;'/>Supprimer cette Maladie</a>";

if(isset($_GET['supprimerMaladie'])){
	MaladieDAO::supprimerUneMaladie($_SESSION['menuMaladie']);
	header("Location: index.php?menuMaladie=1");
}

if(isset($_GET['ajouterMaladie'])){
	header("Location: index.php?ifraMP=ajoutMaladie");
}

if(isset($_GET['modifierMaladie'])){
	header("Location: index.php?ifraMP=modifierMaladie");
}




require_once 'vue/vueMaladies.php' ;