<?php

$tabInfos = PlanteDAO::lire($_SESSION['menuPlante']);

$formPlante = new Formulaire("post", "index.php", "Plante", 'Plante', $messageErreurConn);

$unComposant = $formPlante->creerInputHidden('idPlante','idPlante',$tabInfos[0]);
$formPlante->ajouterComposantLigne($unComposant, 1);
$formPlante->ajouterComposantTab();

$unComposant = $formPlante->creerInputTexteMaxLength('nomPlante', 'nomPlante', 'Nom :',$tabInfos[1] , 1, 'Saisissez le nom de la plante', 0,128);
$formPlante->ajouterComposantLigne($unComposant, 1);
$formPlante->ajouterComposantTab();



$unComposant = $formPlante->creerInputTextArea('descriptifPlante', '10', '155 :',$tabInfos[2],'Description :');
$formPlante->ajouterComposantLigne($unComposant, 1);
$formPlante->ajouterComposantTab();



$unComposant = $formPlante->creerInputSubmit('validerModifPlante', 'validerModifPlante', 'Valider', 'btn btn-primary mt-2 w-100');
$formPlante->ajouterComposantLigne($unComposant, 1);
$formPlante->ajouterComposantTab();


$formPlante->creerFormulaire();

require_once 'vue/modifierPlante.php' ;