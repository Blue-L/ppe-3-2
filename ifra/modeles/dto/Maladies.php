<?php
require_once 'Maladie.php';
class Maladies{
	private $maladies = array();

	public function __construct($array){
		if(is_array($array)){
			$this->maladies = $array;
		}
	}

	public function getMaladies(){
		return $this->maladies;
	}

	public function chercheMaladie($unIdBioAgresseur){
		$i = 0;
		while($i < count($this->maladies)){
			//var_dump($this->plantes[$i]);
			if($unIdBioAgresseur == $this->maladies[$i]->getIdBioAgresseur()){
				return $this->maladies[$i];
			}
		$i++;
	}
	}
}