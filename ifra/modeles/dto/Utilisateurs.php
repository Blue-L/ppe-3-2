<?php

class Utilisateurs{

	private $utilisateurs=array();

	public function __construct($array){
		if (is_array($array)){
			$this->utilisateurs = $array;
		}
	}

	public function getUtilisateurs(){
		return $this->utilisateurs;
	}
}