<?php
require_once 'Plante.php';
class Plantes{
	private $plantes = array();

	public function __construct($array){
		if(is_array($array)){
			$this->plantes = $array;
		}
	}

	public function getPlantes(){
		return $this->plantes;
	}

	public function cherchePlante($unIdPlante){
		$i = 0;
		while($i < count($this->plantes)){
			//var_dump($this->plantes[$i]);
			if($unIdPlante == $this->plantes[$i]->getIdPlante()){
				return $this->plantes[$i];
			}
		$i++;
	}
	}
}
