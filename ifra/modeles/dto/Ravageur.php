<?php
require_once 'Bioagresseur.php' ;

class Ravageur extends BioAgresseur{
use Hydrate;

    private $stadeactif;
    private $nbgenerations;
    
    public function __construct($unIdBioAgresseur = NULL, $unStadeActif = NULL,$unNbGenerations = NULL,$unNomBioagresseur = NULL, $unTypeBioagresseur = NULL, $unePeriodeRisqueBioAgresseur = NULL, $symptomesBioAgresseur = NULL, $stadeSensibleBioAgresseur = NULL){
        parent::__construct($unIdBioAgresseur,$unNomBioagresseur);
        $this->stadeactif = $unStadeActif;
        $this->nbgenerations = $unNbGenerations;
        $this->typeBioagresseur = $unTypeBioagresseur;
        $this->PeriodeRisqueBioAgresseur  = $unePeriodeRisqueBioAgresseur;
        $this->symptomesBioAgresseur = $symptomesBioAgresseur;
        $this->stadeSensibleBioAgresseur = $stadeSensibleBioAgresseur;
    }
    
    public function getStadeActif(){
        return $this->stadeactif;
    }
    
    public function setStadeActif($StadeActif){
        $this->stadeactif = $StadeActif;
    }
    
    public function getNombreGenerations(){
        return $this->nbgenerations;
    }
    
    public function setNombreGenerations($nbgenerations){
        $this->nbgenerations = $nbgenerations;
    }
    
}