<?php 
abstract class BioAgresseur{
	use Hydrate;
	protected $idBioAgresseur;
	protected $nomBioAgresseur;
	protected $typeBioAgresseur;
	protected $periodeRisqueBioAgresseur;
	protected $symptomesBioAgresseur;
	protected $stadeSensibleBioAgresseur;

	protected function __construct($unIdBioagresseur = NULL, $unNomBioAgresseur = NULL){
		$this->idBioAgresseur = $unIdBioagresseur;
		$this->nomBioAgresseur = $unNomBioAgresseur;
	}

	 public function getIdBioAgresseur()
    {
        return $this->idBioAgresseur;
    }

    /**
     * @return the $nomBioAgresseur
     */
    public function getNomBioAgresseur()
    {
        return $this->nomBioAgresseur;
    }

    /**
     * @return the $typeBioAgresseur
     */
    public function getTypeBioAgresseur()
    {
        return $this->typeBioAgresseur;
    }

    /**
     * @return the $periodeRisqueBioAgresseur
     */
    public function getPeriodeRisqueBioAgresseur()
    {
        return $this->periodeRisqueBioAgresseur;
    }

    /**
     * @return the $symptomesBioAgresseur
     */
    public function getSymptomesBioAgresseur()
    {
        return $this->symptomesBioAgresseur;
    }

    /**
     * @return the $stadeSensibleBioAgresseur
     */
    public function getStadeSensibleBioAgresseur()
    {
        return $this->stadeSensibleBioAgresseur;
    }

    /**
     * @param string $idBioAgresseur
     */
    public function setIdBioAgresseur($idBioAgresseur)
    {
        $this->idBioAgresseur = $idBioAgresseur;
    }

    /**
     * @param string $nomBioAgresseur
     */
    public function setNomBioAgresseur($nomBioAgresseur)
    {
        $this->nomBioAgresseur = $nomBioAgresseur;
    }

    /**
     * @param field_type $typeBioAgresseur
     */
    public function setTypeBioAgresseur($typeBioAgresseur)
    {
        $this->typeBioAgresseur = $typeBioAgresseur;
    }

    /**
     * @param field_type $periodeRisqueBioAgresseur
     */
    public function setPeriodeRisqueBioAgresseur($periodeRisqueBioAgresseur)
    {
        $this->periodeRisqueBioAgresseur = $periodeRisqueBioAgresseur;
    }

    /**
     * @param field_type $symptomesBioAgresseur
     */
    public function setSymptomesBioAgresseur($symptomesBioAgresseur)
    {
        $this->symptomesBioAgresseur = $symptomesBioAgresseur;
    }

    /**
     * @param field_type $stadeSensibleBioAgresseur
     */
    public function setStadeSensibleBioAgresseur($stadeSensibleBioAgresseur)
    {
        $this->stadeSensibleBioAgresseur = $stadeSensibleBioAgresseur;
    }




}