<?php
class Departement{
    use Hydrate;
    private $codeDepartement;
    private $nomDepartement;
    
    public function __construct($codeDepartement, $nomDepartement)
    {
        $this->codeDepartement = $codeDepartement;
        $this->nomDepartement = $nomDepartement;
    }
    /**
     * @return the $codeDepartement
     */
    public function getCodeDepartement()
    {
        return $this->codeDepartement;
    }

    /**
     * @return the $nomDepartement
     */
    public function getNomDepartement()
    {
        return $this->nomDepartement;
    }

    /**
     * @param field_type $codeDepartement
     */
    public function setCodeDepartement($codeDepartement)
    {
        $this->codeDepartement = $codeDepartement;
    }

    /**
     * @param field_type $nomDepartement
     */
    public function setNomDepartement($nomDepartement)
    {
        $this->nomDepartement = $nomDepartement;
    }

    
    
}