<?php

class Utilisateur{
    use Hydrate;
    private $idUtilisateur;
    private $login;
    private $mdp;
    private $code;
    
    

    public function __construct($idUtilisateur = NULL, $login = NULL, $codeType = NULL, $mdp = NULL)
    {
        $this->idUtilisateur = $idUtilisateur;
        $this->login = $login;
        $this->codeType=$codeType;
        $this->mdp=$mdp;
    }

    /**
     * Get the value of idUtilisateur
     */ 
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    /**
     * Set the value of idUtilisateur
     *
     * @return  self
     */ 
    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get the value of loginUtilisateur
     */ 
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set the value of loginUtilisateur
     *
     * @return  self
     */ 
    public function setLogin($login)
    {
        $this->login= $login;

        return $this;
    }

    /**
     * Get the value of mdpUtilisateur
     */ 
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdpUtilisateur
     *
     * @return  self
     */ 
    public function setMdp($mdpUtilisateur)
    {
        $this->mdp = $mdpUtilisateur;

        return $this;
    }

    /**
     * Get the value of codeTypeUtilisateur
     */ 
    public function getCodeType()
    {
        return $this->codeType;
    }

    /**
     * Set the value of codeTypeUtilisateur
     *
     * @return  self
     */ 
    public function setCodeType($codeTypeUtilisateur)
    {
        $this->codeType = $codeTypeUtilisateur;

        return $this;
    }
}