<?php
class Organe{
	private $idOrgane;
	private $nomOrgane;

	public function  __construct($unIdOrgane = NULL,$unNomOrgane = NULL){
		$this->idOrgane = $unIdOrgane;
		$this->nomOrgane = $nomOrgane;
	}

	public function getIdOrgane()
    {
        return $this->idOrgane;
    }

    /**
     * @return the $nomOrgane
     */
    public function getNomOrgane()
    {
        return $this->nomOrgane;
    }

    /**
     * @param string $idOrgane
     */
    public function setIdOrgane($idOrgane)
    {
        $this->idOrgane = $idOrgane;
    }

    /**
     * @param field_type $nomOrgane
     */
    public function setNomOrgane($nomOrgane)
    {
        $this->nomOrgane = $nomOrgane;
    }

}