<?php
class Plante{
    use Hydrate;
    private $idPlante;
    private $nomPlante;
    private $descriptifPlante;
    private $lesMaladies = [];
    private $lesRavageurs = [];
    
    public function __construct($unIdPlante=NULL , $unNomPlante=NULL, $unDescriptifPlante=NULL){
        $this->idPlante = $unIdPlante;
        $this->nomPlante = $unNomPlante;
        $this->descriptifPlante= $unDescriptifPlante;
    }
    /**
     * @return the $idPlante
     */
    public function getIdPlante()
    {
        return $this->idPlante;
    }

    /**
     * @return the $nomPlante
     */
    public function getNomPlante()
    {
        return $this->nomPlante;
    }

    /**
     * @return the $descriptifPlante
     */
    public function getDescriptifPlante()
    {
        return $this->descriptifPlante;
    }

    /**
     * @return the $lesMaladies
     */
    public function getLesMaladies()
    {
        return $this->lesMaladies;
    }

    /**
     * @return the $lesRavageurs
     */
    public function getLesRavageurs()
    {
        return $this->lesRavageurs;
    }

    /**
     * @param string $idPlante
     */
    public function setIdPlante($idPlante)
    {
        $this->idPlante = $idPlante;
    }

    /**
     * @param string $nomPlante
     */
    public function setNomPlante($nomPlante)
    {
        $this->nomPlante = $nomPlante;
    }

    /**
     * @param string $descriptifPlante
     */
    public function setDescriptifPlante($descriptifPlante)
    {
        $this->descriptifPlante = $descriptifPlante;
    }

    /**
     * @param multitype: $lesMaladies
     */
    public function setLesMaladies($lesMaladies)
    {
        $this->lesMaladies = $lesMaladies;
    }

    /**
     * @param multitype: $lesRavageurs
     */
    public function setLesRavageurs($lesRavageurs)
    {
        $this->lesRavageurs = $lesRavageurs;
    }

    
}