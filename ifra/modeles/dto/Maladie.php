<?php
require_once 'Bioagresseur.php' ;

class Maladie extends BioAgresseur{
use Hydrate;

	private $conditionsFavorables;

	public function __construct($unIdBioAgresseur = NULL, $desConditionsFavorables = NULL,$unNomBioagresseur = NULL, $unTypeBioagresseur = NULL, $unePeriodeRisqueBioAgresseur = NULL, $symptomesBioAgresseur = NULL, $stadeSensibleBioAgresseur = NULL){
		parent::__construct($unIdBioAgresseur,$unNomBioagresseur);
		$this->conditionsFavorables = $desConditionsFavorables;
		$this->typeBioagresseur = $unTypeBioagresseur;
		$this->PeriodeRisqueBioAgresseur  = $unePeriodeRisqueBioAgresseur;
		$this->symptomesBioAgresseur = $symptomesBioAgresseur;
		$this->stadeSensibleBioAgresseur = $stadeSensibleBioAgresseur;
	}

	public function getConditionsFavorables(){
		return $this->conditionsFavorables;
	}

	public function setConditionsFavorables($conditionsFavorables){
		$this->conditionsFavorables = $conditionsFavorables;
	}

}