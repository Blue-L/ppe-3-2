<?php
require_once 'Ravageur.php';
class Ravageurs{
	private $Ravageurs = array();

	public function __construct($array){
		if(is_array($array)){
			$this->Ravageurs = $array;
		}
	}

	public function getRavageurs(){
		return $this->Ravageurs;
	}

	public function chercheRavageur($unIdBioAgresseur){
		$i = 0;
		while($i < count($this->Ravageurs)){
			//var_dump($this->plantes[$i]);
			if($unIdBioAgresseur == $this->Ravageurs[$i]->getIdBioAgresseur()){
				return $this->Ravageurs[$i];
			}
		$i++;
	}
	}
}