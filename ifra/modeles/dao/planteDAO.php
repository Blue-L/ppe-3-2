<?php
require_once 'modeles/dto/Plante.php';
class PlanteDAO{
    
    
        
        public static function lire($IdPlante){
            $sql = "select * from PLANTE where idPlante = '".$IdPlante."'";
            $plante = dBConnex::getInstance()->queryFetchFirstRow($sql);
            return $plante;
        }
        
        public static function ajouterPlante($nomPlante, $descriptifPlante)
        {
            $sql = "insert into PLANTE values (DEFAULT,'".$nomPlante."', '".$descriptifPlante."')";
            $sth = dBConnex::getInstance()->insert($sql);
            return $sth;
        }
        
        public static function supprimerPlante($Idplante)
        {
            $sql = "delete from PLANTE where idPlante ='" . $Idplante."'";
            //var_dump($sql);
            $plante = dBConnex::getInstance()->delete($sql);
            return $plante;
        }
        
        public static function modifierPlante($idPlante, $nomPlante, $descriptifPlante)
        {
            $sql = "update PLANTE set nomPlante='" . $nomPlante . "',descriptifPlante='" . $descriptifPlante . "' where idPlante='" . $idPlante . "';";
            //var_dump($sql);
            $plante = dBConnex::getInstance()->update($sql);
            return $plante;
        }

        public static function lesPlantes(){
            $tabPlantes = [];
            $sql = "select * from PLANTE";
            $reqPrepa = dBConnex::getInstance()->prepare($sql);
            $reqPrepa->execute();
            $listePlante = $reqPrepa->fetchAll(PDO::FETCH_ASSOC);
            foreach ($listePlante as $plante) {
                $unePlante = new Plante();
                $unePlante->hydrate($plante);
                $tabPlantes[] = $unePlante;
            }
            return $tabPlantes;
        }
        
        public static function lesPlantesBioAgresseurs(){
            $tabPlantes = [];
            $tabMaladies = [];
            $tabRavageurs = [];
            $sql = "Select * from PLANTE WHERE idPlante";
            $reqPrepa = dBConnex::getInstance()->prepare($sql);
            $sqlMala = "Select MALADIE.* from MALADIE,BIOAGRESSEUR,SENSIBLE where MALADIE.idBioAgresseur = BIOAGRESSEUR.idBioAgresseur and BIOAGRESSEUR.idBioAgresseur = SENSIBLE.idBioAgresseur and idPlante = :idPlante ";
            //$sqlMala = "select * from Maladie WHERE idBioAgresseur IN (SELECT idBioAgresseur from SENSIBLE WHERE idPlante = :idPlante";
            $reqMala = dBConnex::getInstance()->prepare($sqlMala);
            $sqlRava = "Select RAVAGEUR.* from RAVAGEUR,BIOAGRESSEUR,SENSIBLE where RAVAGEUR.idBioAgresseur = BIOAGRESSEUR.idBioAgresseur and BIOAGRESSEUR.idBioAgresseur = SENSIBLE.idBioAgresseur and idPlante = :idPlante";
            $reqRava = dBConnex::getInstance()->prepare($sqlRava);

            $reqPrepa->execute();
            $liste = $reqPrepa->fetchAll(PDO::FETCH_ASSOC);
            if(!empty($liste)){
                foreach ($liste as $plante) {
                    $unePlante = new Plante();
                    $unePlante->hydrate($plante);
                    $unId = $unePlante->getIdPlante();
                    $reqMala->bindParam(":idPlante",$unId);
                    $reqMala->execute();
                    $listeMala = $reqMala->fetchAll(PDO::FETCH_ASSOC);
                    //var_dump($listeMala);
                    if(!empty($listeMala)){
                        foreach ($listeMala as $maladie) {
                            $uneMaladie = new Maladie();
                            $uneMaladie->hydrate($maladie);
                            $tabMaladies[] = $uneMaladie;

                        }
                        //var_dump($tabMaladies);
                        $unePlante->setLesMaladies($tabMaladies);
                    }
                    $unId = $unePlante->getIdPlante();
                    $reqRava->bindParam(":idPlante",$unId);
                    $reqRava->execute();
                    $listeRava = $reqRava->fetchAll(PDO::FETCH_ASSOC);
                    //var_dump($listeRava);
                    if(!empty($listeRava)){
                        foreach ($listeRava as $Rava) {
                           $unRavageur = new Ravageur();
                           $unRavageur->hydrate($Rava);
                           $tabRavageurs[] = $unRavageur;
                        }
                        //var_dump($tabRavageurs);
                        $unePlante->setLesRavageurs($tabRavageurs);
                    }
                    $tabPlantes[] = $unePlante;
                }
            }
            return $tabPlantes;
        }

        public static function lesMaladiesPlante($unIdPlante){
            $tabMaladies = [];
            $sql = "select * from MALADIE WHERE idBioAgresseur IN (SELECT idBioAgresseur from SENSIBLE WHERE idPlante = :idPlante";
            $reqPrepa = dBConnex::getInstance()->prepare($sql);
            $reqPrepa->bindParam(":idPlante",$unIdPlante);
            $reqPrepa->execute();
            $listePlante = $reqPrepa->fetchAll(PDO::FETCH_ASSOC);
            foreach ($listeMala as $mala) {
                $uneMaladie = new Maladie();
                $uneMaladie->hydrate($mala);
                $tabMaladies[] = $uneMaladie;
            }
            return $tabMaladies;
        }
    
    public static function planteSensible($idPlante,$idBioAgresseur){
        $sql = "insert into SENSIBLE values('".$idBioAgresseur."','".$idPlante."');";
            //var_dump($sql);
            $plante = dBConnex::getInstance()->update($sql);
            //var_dump($sql);
            return $plante;
    }
    
}