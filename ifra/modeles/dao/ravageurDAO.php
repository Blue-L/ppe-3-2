<?php
class RavageurDAO{
    public static function lesRavageurs(){
		$result = [];
		$sql = "select * from RAVAGEUR";
		$requetePrepa = dBConnex::getInstance()->prepare($sql);
		$requetePrepa->execute();
		$liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
		if(!empty($liste)){
		    foreach($liste as $Ravageur){
		        $unRavageur = new Ravageur();
		        $unRavageur->hydrate($Ravageur);
		        $result[] = $unRavageur;
		}
		//var_dump($result);
	}
	return $result;
}
	public static function supprimerUnRavageur($unIdRavageur){

		$sqlSuppr = "DELETE FROM RAVAGEUR WHERE idBioAgresseur = '".$unIdRavageur."'";
		DBConnex::getInstance()->exec($sqlSuppr);
		$sqlSuppr = "DELETE FROM SENSIBLE WHERE idBioAgresseur = '".$unIdRavageur."'";
		DBConnex::getInstance()->exec($sqlSuppr);
		$sqlSuppr = "DELETE FROM BIOAGRESSEUR WHERE idBioAgresseur = '".$unIdRavageur."'";
		DBConnex::getInstance()->exec($sqlSuppr);

	}

	public static function ajouterRavageur($nomRavageur,$stadeActifRavageur,$nbGenerations,$periodeRisqueRavageur,$symptomesBioAgresseur,$stadeSensibleRavageur){
		$sql = "BEGIN;";
		$sql .="insert into BIOAGRESSEUR values (DEFAULT,'".$nomRavageur."','M','".$periodeRisqueRavageur."','".$symptomesBioAgresseur."','".$stadeSensibleRavageur."');";
		$sql .="insert into RAVAGEUR values (LAST_INSERT_ID(),'".$stadeActifRavageur."','".$nbGenerations."', '".$nomRavageur."','R','".$periodeRisqueRavageur."','".$symptomesBioAgresseur."','".$stadeSensibleRavageur."');";
		$sql .="COMMIT;";
		//var_dump($sql);
        $sth = dBConnex::getInstance()->exec($sql);
    }

}
