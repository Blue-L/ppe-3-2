<?php
use Hydrate;

class UtilisateurDAO{
public static function lesOrganes(){
	$result = [];
	$sql = "Select * from ORGANES, AFFECTER where ORGANES.idOrgane = AFFECTER.idOrgane";
	$reqPrepa = dBConnex::getInstance()->prepare($sql);
	$reqPrepa->execute;
	$liste = $reqPrepa->fetchAll(PDO::FETCH_ASSOC);
	if(!empty($liste)){
		foreach ($liste as $organe) {
			$unOrgane = new Organe();
			$unOrgane->hydrate($organe);
			$result[] = $unOrgane;
		}
	}
	return $result;
}
}