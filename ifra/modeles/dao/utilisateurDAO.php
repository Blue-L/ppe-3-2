<?php 

class UtilisateurDAO{

    public static function identification($login, $mdp)
    {
        $sql = "select count(*) from UTILISATEUR where LOGIN = '" . $login . "' and MDP = '" . md5($mdp) . "'";
        $valide = dBConnex::getInstance()->queryFetchFirstRow($sql);
        if ($valide[0] == 1) {
            return true;
        }
        
        return false;
    }

    public static function getNumUtilisateur($login, $mdp)
    {
        $sql = "select idUtilisateur from UTILISATEUR where login = '".$login."' and mdp = '" . md5($mdp) . "'";
        $valide = dBConnex::getInstance()->queryFetchFirstRow($sql);
        return $valide[0];
    }
    public static function getTypeUtilisateur($numUtilisateur)
    {
        $sql = "select codeType from UTILISATEUR where idUtilisateur = '" . $numUtilisateur . "'";
        $valide = dBConnex::getInstance()->queryFetchFirstRow($sql);
        return $valide[0];
    }
	
	public static function recupUtilisateurs(){
        $result=[];
        $sql="select * from UTILISATEUR";
        $requete=dBConnex::getInstance()->prepare($sql);
		$requete->execute();
        $requete = $requete->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($requete)){
            foreach($requete as $utilisateur){
                $unUtilisateur= new Utilisateur();
                $unUtilisateur->hydrate($utilisateur);
                $result[]=$unUtilisateur;

            }
        }
        //print_r($result);
        return $result;
    }



     static function insertUtilisateur($id,$codeType,$login,$mdp){
        $sql="insert into UTILISATEUR values(:id,:CT,:login,:mdp)";
        $requete=dBConnex::getInstance()->prepare($sql);
        $requete->bindParam(":id",$id);
        $requete->bindParam(":CT",$codeType);
        $requete->bindParam(":login",$login);
        $requete->bindParam(":mdp",$mdp);
        $requete->execute();
    } 

    static function deleteUtilisateur($id){
        $sql="delete from UTILISATEUR where idUtilisateur = :id";
        $requete=dBConnex::getInstance()->prepare($sql);
        $requete->bindParam(":id",$id);
        $requete->execute();
    }


}